package ntq.solution.greff.project.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {
    public static final String DEFAULT_SHORTDATEFORMAT = "dd/MM/yyyy";
    public static final String DEFAULT_SIMPLEDATEFORMAT = "yyyy-MM-dd";
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat(DEFAULT_SIMPLEDATEFORMAT);

    public static final SimpleDateFormat TIME = new SimpleDateFormat("hh:mm");
    public static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-mm-yy");

    public static Date convertFromShortString(final String date) throws ParseException {
        return SHORT_DATE_FORMAT.parse(date);
    }


    public static long convertToPETime(final Date date) {
        final long value = date.getTime();
        return value / 1000;
    }

    public static String getDate(final Date date) {
        return DATE.format(date);
    }

    public static String getTime(final Date date) {
        return TIME.format(date);
    }

    public static long dateTolong(final String strDate) throws ParseException {
        Date date = TIME.parse(strDate);
        return date.getTime();
    }

    public static String dateToString(Date date, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String strDate = simpleDateFormat.format(date);
        return strDate;
    }
}
