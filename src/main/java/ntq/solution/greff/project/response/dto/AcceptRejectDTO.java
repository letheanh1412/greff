package ntq.solution.greff.project.response.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AcceptRejectDTO {
    private Long id;
    private Integer status;
}
