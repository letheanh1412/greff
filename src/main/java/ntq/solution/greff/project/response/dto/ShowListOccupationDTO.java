package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class ShowListOccupationDTO {
    private Long id;
    private String title;
}
