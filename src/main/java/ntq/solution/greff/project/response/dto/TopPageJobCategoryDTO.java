package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class TopPageJobCategoryDTO {
    private String id;
    private String categoryName;
    private String description;
    private Long jobTotal;
}
