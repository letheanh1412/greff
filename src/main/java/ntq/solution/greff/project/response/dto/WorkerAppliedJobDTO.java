package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkerAppliedJobDTO {
    private Long id;
    private String title;
    private String workDate;
    private String workingTimeStart;
    private String workingTimeEnd;
    private Integer numberOfPeople;
    private Integer applied;
    private String avatar;
    private String name;
    private String furiganaName;
    private Integer age;
    private String gender;
    private String status;
    private String totalNumberOfJob;
    private String totalTimeOfJobs;
}
