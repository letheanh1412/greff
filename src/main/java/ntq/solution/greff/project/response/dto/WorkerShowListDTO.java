package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkerShowListDTO {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String invitationCode;
    private boolean status;
    private String createdDate;
    private long workerTotal;
    private long workerTotalInToDay;
}
