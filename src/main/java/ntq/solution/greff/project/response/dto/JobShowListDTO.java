package ntq.solution.greff.project.response.dto;

import lombok.Data;

import java.util.List;

@Data
public class JobShowListDTO {
    private Long id;
    private String breakTime;
    private Integer numberOfPeople;
    private Double salaryPerHours;
    private Integer travelFees;
    private String deadlineToApplyDateTime;
    private String recruitmentStatus;
    private String workTime;
    private String workDate;
    private String workingTimeStart;
    private String workingTimeEnd;
    private String title;
    private Integer appliedPeople;
    private Long occupationId;
    private String workAddress;
    private String settingMatchingJob;
    private String accessAddress;
    private List<String> linkFile;
}
