package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class RouteDTO {
    private Long id;
    private String name;
    private String status;
    private String code;
    private String corporationId;
}
