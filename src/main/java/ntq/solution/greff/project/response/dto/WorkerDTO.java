package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkerDTO {
    private Long id;
    private String name;
    private String furiganaName;
    private String phoneNumber;
    private String email;
    private String area;
}
