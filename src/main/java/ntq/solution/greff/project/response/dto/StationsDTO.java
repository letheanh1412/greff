package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class StationsDTO {
    private Long id;
    private String name;
    private String katakanaName;
    private String kanjiName;
    private String yogi;
    private String type;
    private String gcs;
    private String latiD;
    private String longiD;
    private String lati;
    private String longi;
    private String code;
}
