package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetailWorkerAppliedJobDTO {
    private String avatar;
    private String name;
    private String furiganaName;
    private Integer age;
    private String gender;
    private String totalNumberOfJob;
    private String totalTimeOfJobs;
    private String email;
    private String phoneNumber;
    private String address;
    private String areaWantToWork;
    private List<String> certificate;
}
