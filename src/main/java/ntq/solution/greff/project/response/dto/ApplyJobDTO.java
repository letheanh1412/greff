package ntq.solution.greff.project.response.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApplyJobDTO {
    private String name;
    private String furiganaName;
    private String phoneNumber;
    private String email;
    private Long jobId;
    private Long workerId;
}
