package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ntq.solution.greff.project.entity.User;

import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String name;
    private String email;
    private Date createdAt;
    private boolean status;
    private boolean isAccountLocked;
    private String roles;

    public static UserDTO fromEntity(User user, String roles) {
        return UserDTO.builder()
                .name(user.getName())
                .email(user.getEmail())
                .createdAt(user.getCreatedAt())
                .status(user.getStatus())
                .isAccountLocked(user.isAccountLocked())
                .roles(roles)
                .build();
    }
}
