package ntq.solution.greff.project.response.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JobDetailDTO {
    private String workDateTime;
    private String workingTime;
    private String titleOccupation;
    private Double amountTotalJob;
    private String descriptionOccupation;
    private String workTime;
    private Double salaryPerHours;
    private Integer travelFees;
    private String breakTime;
    private String jobCategory;
    private String bringItems;
    private String workAddress;
    private String skillRequired;
    private String note;
    private String fileName;
    private String deadlineToApplyDateTime;
}
