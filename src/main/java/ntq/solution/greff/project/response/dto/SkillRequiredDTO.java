package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class SkillRequiredDTO {
    private String nameSkill;
}
