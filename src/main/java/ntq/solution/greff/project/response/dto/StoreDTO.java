package ntq.solution.greff.project.response.dto;

import lombok.Data;

import java.util.List;

@Data
public class StoreDTO {
    private Long id;
    private String storeName;
    private String storeNameKana;
    private String address;
    private String emailCurator;
    private String nameCurator;
    private String phoneCurator;
    private boolean status;
    private String hpUrl;
    private String district;
    private List<String> stations;
}
