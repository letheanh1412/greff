package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class DistrictDto {
    private Long id;
    private String katakanaName;
    private String kanjiName;
}
