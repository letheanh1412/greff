package ntq.solution.greff.project.response;

import lombok.Data;

@Data
public class RouteResponse {
    private Long id;
    private String name;
    private String status;
    private String code;
    private long totalJob;
}
