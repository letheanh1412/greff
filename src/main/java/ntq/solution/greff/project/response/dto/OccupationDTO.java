package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ntq.solution.greff.project.entity.JobCategory;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OccupationDTO {
    private Long id;
    private String title;
    private String description;
    private String workAddress;
    private String accessAddress;
    private String note;
    private String bringItems;
    private boolean status;
    private JobCategory jobCategory;
    private List<String> files;
    private List<SpecialityDTO> specialityDTOS;
    private List<SkillRequiredDTO> skillRequiredDTOS;
    private List<RouteDTO> routeDTOS;
}
