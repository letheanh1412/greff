package ntq.solution.greff.project.response;

import lombok.Data;

@Data
public class CityResponse {
    private long id;
    private String nameArea;
    private String katakanaName;
    private String kanjiName;
    private Integer level;
    private Long parentId;
    private long totalJob;
}
