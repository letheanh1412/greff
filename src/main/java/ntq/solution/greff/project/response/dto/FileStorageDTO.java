package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class FileStorageDTO {
    private Long id;
    private String link;
    private String name;
    private String type;
}
