package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class CompanyShowListDto {
    private Long id;
    private String companyName;
    private String hpUrl;
    private String nameContact;
    private String phoneContact;
    private String emailContact;
    private String address;
    private Boolean status;
    private String createdDate;
}
