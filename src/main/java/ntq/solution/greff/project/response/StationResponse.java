package ntq.solution.greff.project.response;

import lombok.Data;

@Data
public class StationResponse {
    private long id;
    private String name;
    private String katakanaName;
    private String kanjiName;
    private String yogi;
    private long totalJob;
}
