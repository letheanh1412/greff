package ntq.solution.greff.project.response.dto;

import lombok.Data;

import java.util.List;

@Data
public class CompanyDetailDto {
    private Long Id;
    private String companyName;
    private String companyNameKana;
    private String registerName;
    private String registerNameKana;
    private String room;
    private String building;
    private String zipcode;
    private String hpUrl;
    private String nameContact;
    private String phoneContact;
    private String emailContact;
    private Boolean status;
    private String note;
    private String reason;
    private String city;
    private String district;
    List<String> listAreaRequests;
    List<String> listJobCategoryRequests;
}
