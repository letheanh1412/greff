package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class OccupationStationDTO {
    private String name;
}
