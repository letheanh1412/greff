package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class JobConfirmDTO {
    private Long id;
    private String status;
    private String title;
    private Integer numberOfPeople;
    private Long occupationId;
    private String breakTime;
    private String deadlineToApplyDate;
    private String deadlineToApplyTime;
    private Double salaryPerHours;
    private Integer travelFees;
}
