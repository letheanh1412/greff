package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class AreaDTO {
    private Long id;
    private String nameArea;
    private String katakanaName;
    private String kanjiName;
    private Integer level;
    private Long parentId;
}
