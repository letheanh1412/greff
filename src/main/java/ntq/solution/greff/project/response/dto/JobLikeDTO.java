package ntq.solution.greff.project.response.dto;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class JobLikeDTO {
    private Long id;
    private Long jobId;
}
