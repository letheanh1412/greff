package ntq.solution.greff.project.response.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateCartDTO {
    private String frontOfIdentityCard;
    private String afterOfIdentityCard;
}
