package ntq.solution.greff.project.response.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkerDetailDTO {
    private String avatar;
    private String name;
    private String furiganaName;
    private String gender;
    private Date birthday;
    private String areaWantToWork;
    private String email;
    private String phoneNumber;
    private List<String> certificate;
    private String frontOfIdentityCard;
    private String afterOfIdentityCard;
}
