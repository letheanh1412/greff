package ntq.solution.greff.project.response.dto;

import lombok.Data;

@Data
public class MyPageDTO {
    private String avatar;
    private String firstName;
    private String lastName;
    private double totalTime;
    private int penalty;
    private String firstFuriganaName;
    private String lastFuriganaName;
    private String gender;
    private String dateOfBirth;
    private String phoneNumber;
    private Long areaId;
    private String email;
}
