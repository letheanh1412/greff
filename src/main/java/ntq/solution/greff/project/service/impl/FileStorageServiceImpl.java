package ntq.solution.greff.project.service.impl;

import ntq.solution.greff.project.entity.FileStorage;
import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.repository.FileStorageRepository;
import ntq.solution.greff.project.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final FileStorageRepository fileStorageRepository;

    public FileStorageServiceImpl(FileStorageRepository fileStorageRepository) {
        this.fileStorageRepository = fileStorageRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FileStorage storeFile(MultipartFile file, Occupation occupation) {
        try {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            FileStorage fileStorage = FileStorage.builder()
                    .link("/api/file/image/")
                    .name(fileName)
                    .type(file.getContentType())
                    .photos(file.getBytes())
                    .occupation(occupation)
                    .build();
            return fileStorageRepository.save(fileStorage);
        } catch (IOException e) {
            logger.error(String.format("Upload file that thread [%s]", e));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
