package ntq.solution.greff.project.service;

import ntq.solution.greff.project.response.dto.UploadFileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface DocumentStorageService {
    String storeFile(MultipartFile file);

    List<UploadFileDTO> uploadMultipleFiles(MultipartFile[] files);
}