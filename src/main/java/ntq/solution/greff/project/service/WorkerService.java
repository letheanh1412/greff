package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;


@Service
public interface WorkerService {
    WorkerDTO createWorker(CreateWorkerRequest createWorkerRequest) throws Exception;

    WorkerDTO checkCodeConfirm(Long id, CheckCodeRequest checkCodeRequest) throws Exception;

    WorkerDTO setPassword(Long id, UpdatePassword updatePassword) throws Exception;

    WorkerDTO getMailMessage(ForgotPasswordWorkerRequest forgotPasswordWorkerRequest);

    WorkerDTO sendCodeAgain(CreateWorkerRequest createWorkerRequest);

    Page<WorkerShowListDTO> searchWorker(String search, String startDate, String endDate, Pageable pageable) throws Exception;

    boolean deleteWorker(Long id);

    WorkerDetailDTO detailWorker(Long id);

    WorkerDetailDTO editWorker(Long id, EditWorkerRequest editWorkerRequest) throws ParseException;

    WorkerDetailDTO createWorkerByAdmin(CreateWorkerByAdmin createWorkerByAdmin);

    UpdateCartDTO updateCard(UpdateCardRequest updateCardRequest);

    ApproveRejectDTO StatusConfirmation(Integer StatusConfirmation, Long idWorker);

    ApplyJobDTO applyJob(ApplyJobRequest applyJobRequest);

    MyPageDTO infoWorker();

    MyPageDTO updateWorker(UpdateWorkerRequest updateWorkerRequest) throws ParseException;
}
