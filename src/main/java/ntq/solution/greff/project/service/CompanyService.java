package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.CompanyDetailDto;
import ntq.solution.greff.project.response.dto.CompanyShowListDto;
import ntq.solution.greff.project.response.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;

public interface CompanyService {
    void generateCsvResponse(HttpServletResponse response);

    ResponseEntity<Page<CompanyShowListDto>> search(SearchCompanyRequest searchCompanyRequest, Pageable pageable);

    ResponseEntity<CompanyShowListDto> create(CreateCompanyRequest searchCompanyRequest) throws Exception;

    ResponseEntity<CompanyShowListDto> edit(Long id, UpdateCompanyRequest updateCompanyRequest) throws Exception;

    CompanyDetailDto details(Long id);

    Page<UserDTO> getUserStore(Pageable pageable);

    UserDTO createUserCompany(CreateUserRequest createUserRequest) throws Exception;

    UserDTO updateUserCompany(String email, UpdateUserRequest updateUserRequest) throws Exception;

    boolean deleteUserCompany(String email) throws Exception;

    UserDTO detailUserCompany(String email);

    Page<UserDTO> getAllAccountCompany(Pageable pageable);
}
