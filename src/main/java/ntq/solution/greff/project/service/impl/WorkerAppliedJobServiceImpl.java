package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.response.dto.AcceptRejectDTO;
import ntq.solution.greff.project.response.dto.DetailWorkerAppliedJobDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.response.dto.WorkerAppliedJobDTO;
import ntq.solution.greff.project.service.WorkerAppliedJobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WorkerAppliedJobServiceImpl implements WorkerAppliedJobService {
    private final UserJobRepository userJobRepository;
    private final WorkerRepository workerRepository;
    private final JobRepository jobRepository;
    private final UserRepository userRepository;
    private final OccupationRepository occupationRepository;
    private final AreaRepository areaRepository;
    private final DegreeWorkerRepository degreeWorkerRepository;

    public WorkerAppliedJobServiceImpl(UserJobRepository userJobRepository,
                                       WorkerRepository workerRepository,
                                       JobRepository jobRepository,
                                       UserRepository userRepository,
                                       OccupationRepository occupationRepository,
                                       AreaRepository areaRepository,
                                       DegreeWorkerRepository degreeWorkerRepository) {
        this.userJobRepository = userJobRepository;
        this.workerRepository = workerRepository;
        this.jobRepository = jobRepository;
        this.userRepository = userRepository;
        this.occupationRepository = occupationRepository;
        this.areaRepository = areaRepository;
        this.degreeWorkerRepository = degreeWorkerRepository;
    }


    @Override
    public Page<WorkerAppliedJobDTO> showListWorkerAppliedJob(Long id,
                                                              Pageable pageable) {
        Page<UserJob> userJobPage = userJobRepository.findAllByJobId(id, pageable);
        return userJobPage.map(userJob -> {
            Optional<UserJob> userJobOptional = userJobRepository.findById(userJob.getId());
            if (!userJobOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Worker applied does not exist");
            }
            Optional<Job> jobOptional = jobRepository.findById(userJobOptional.get().getJob().getId());
            if (!jobOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Job does not exist");
            }
            Optional<Worker> workerOptional = workerRepository.findById(userJobOptional.get().getWorker().getId());
            if (!workerOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Worker does not exist");
            }
            String fileDownloadUriAvatar = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/download/")
                    .path(workerOptional.get().getAvatar())
                    .toUriString();

            WorkerAppliedJobDTO workerAppliedJobDTO = WorkerAppliedJobDTO.builder()
                    .id(userJob.getId())
                    .title(jobOptional.get().getTitle())
//                        .workDate(DateTimeUtil.dateToString(jobOptional.get().getWorkDate(), "yyyy-MM-dd (E)"))
//                        .workingTimeStart(jobOptional.get().getWorkingTimeStart())
//                        .workingTimeEnd(jobOptional.get().getWorkingTimeEnd())
                    .numberOfPeople(jobOptional.get().getNumberOfPeople())
                    .applied(123)
                    .avatar(fileDownloadUriAvatar)
                    .name(userJobOptional.get().getName())
                    .furiganaName(userJobOptional.get().getFuriganaName())
//                        .age(workerOptional.get().getBirthday())
                    .gender(workerOptional.get().getGender())
//                        .status()
//                        .totalNumberOfJob()
//                        .totalTimeOfJobs()
                    .build();
            log.info("Show list worker applied job successfully");
            return workerAppliedJobDTO;
        });
    }


    @Override
    public Page<JobShowListDTO> showListJobApply(String email, Integer statusApply, Pageable pageable) {
        User user = userRepository.findByEmail(email);
        Worker worker = workerRepository.findByUser(user);
        Page<UserJob> userJobPage = userJobRepository.findAllByWorkerUserEmailAndStatusApplyOrderByJobWorkDateAsc
                (worker.getUser().getEmail(), statusApply, pageable);
        Page<JobShowListDTO> jobShowListDTOS = userJobPage.map(userJob -> getJobShowListDTO(userJobPage, userJob));
        log.info("getList Job success");
        return jobShowListDTOS;

    }

    private JobShowListDTO getJobShowListDTO(Page<UserJob> userJobPage, UserJob userJob) {
        JobShowListDTO jobShowListDTO = ModelMapperUtils.map(userJob.getJob(), JobShowListDTO.class);
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("ja"));
        String date = df.format(userJob.getJob().getWorkDate());
        String deadlineToApplyDateTime = df.format(userJob.getJob().getWorkDate());
        String dateTime = date + " " +
                userJob.getJob().getWorkingTimeStart() + " - " + userJob.getJob().getWorkingTimeEnd();
        for (UserJob job : userJobPage) {
            Optional<Occupation> occupation = occupationRepository.findById(job.getJob().getOccupation().getId());
            if (!occupation.isPresent()) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Occupation Does not exist!");
            }
            List<FileStorage> fileStorages = occupation.get().getFileStorages();
            List<String> linkFiles = fileStorages.stream().map(fileStorage -> ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/api/download/")
                    .path(fileStorage.getName())
                    .toUriString()).collect(Collectors.toList());
            jobShowListDTO.setLinkFile(linkFiles);

            jobShowListDTO.setWorkAddress(occupation.get().getWorkAddress());
            jobShowListDTO.setAccessAddress(occupation.get().getAccessAddress());
        }
        jobShowListDTO.setWorkTime(dateTime);
        jobShowListDTO.setWorkDate(date);
        jobShowListDTO.setDeadlineToApplyDateTime(deadlineToApplyDateTime);
        return jobShowListDTO;
    }

    @Override
    public DetailWorkerAppliedJobDTO detailWorkerAppliedJob(Long id) {
        Optional<UserJob> userJobOptional = userJobRepository.findById(id);
        if (!userJobOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Woker applied does not exist");
        }
        Optional<Worker> workerOptional = workerRepository.findById(userJobOptional.get().getWorker().getId());
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Worker does not exist");
        }
        Optional<User> userOptional = userRepository.findById(workerOptional.get().getUser().getEmail());
        if (!userOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "User does not exist");
        }
        Optional<Job> jobOptional = jobRepository.findById(userJobOptional.get().getJob().getId());
        if (!jobOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Job does not exist");
        }
        Optional<Area> areaOptional = areaRepository.findById(workerOptional.get().getAreaId().getId());
        if (!areaOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Area does not exist");
        }
        String fileDownloadUriAvatar = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(workerOptional.get().getAvatar())
                .toUriString();

        List<String> fileNameURLCertificate = new ArrayList<>();
        List<DegreeWorker> degreeWorkerList = degreeWorkerRepository.findAllByWorkerId(workerOptional.get().getId());
        for (DegreeWorker item : degreeWorkerList) {
            String fileDownloadUriCertificate = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/download/")
                    .path(item.getFileName())
                    .toUriString();
            fileNameURLCertificate.add(fileDownloadUriCertificate);
        }
        DetailWorkerAppliedJobDTO detailWorkerAppliedJobDTO = DetailWorkerAppliedJobDTO.builder()
                .avatar(fileDownloadUriAvatar)
                .name(userJobOptional.get().getName())
                .furiganaName(userJobOptional.get().getFuriganaName())
                .age(new Date().getYear() - workerOptional.get().getBirthday().getYear())
                .gender(workerOptional.get().getGender())
//                .totalNumberOfJob()
//                .totalTimeOfJobs()
                .email(userOptional.get().getEmail())
                .phoneNumber(workerOptional.get().getPhoneNumber())
//                .address()
                .areaWantToWork(areaOptional.get().getName())
                .certificate(fileNameURLCertificate)
                .build();
        log.info("View worker details applied successfully");
        return detailWorkerAppliedJobDTO;
    }

    @Override
    public AcceptRejectDTO statusApplyJob(Integer statusApplyJob, Long id) {
        Optional<UserJob> userJobOptional = userJobRepository.findById(id);
        if (!userJobOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Id does not exist");
        }
        userJobOptional.get().setStatusApply(statusApplyJob);
        userJobRepository.save(userJobOptional.get());
        AcceptRejectDTO acceptRejectDTO = AcceptRejectDTO.builder()
                .id(id)
                .status(statusApplyJob)
                .build();
        log.info("Change status apply job success");
        return acceptRejectDTO;
    }
}
