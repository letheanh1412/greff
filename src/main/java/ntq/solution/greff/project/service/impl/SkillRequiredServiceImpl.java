package ntq.solution.greff.project.service.impl;

import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.entity.SkillRequired;
import ntq.solution.greff.project.repository.SkillRequiredRepository;
import ntq.solution.greff.project.service.SkillRequiredService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class SkillRequiredServiceImpl implements SkillRequiredService {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final SkillRequiredRepository skillRequiredRepository;

    public SkillRequiredServiceImpl(SkillRequiredRepository skillRequiredRepository) {
        this.skillRequiredRepository = skillRequiredRepository;
    }

    @Override
    public SkillRequired saveSkill(String skillRequired, Occupation occupation) {
        try {
            SkillRequired skillRequired1 = SkillRequired.builder()
                    .nameSkill(skillRequired)
                    .occupation(occupation)
                    .build();
            return skillRequiredRepository.save(skillRequired1);
        } catch (Exception e) {
            logger.error(String.format("Failed to save Skill [%s]", e));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
