package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.CompanyCSV;
import ntq.solution.greff.project.response.dto.CompanyDetailDto;
import ntq.solution.greff.project.response.dto.CompanyShowListDto;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.CompanyService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class CompanyServiceImpl implements CompanyService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder passwordEncoder;
    private final CompanyRepository companyRepository;
    private final CompanyJobCategoryRepository companyJobCategoryRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final EntityManager entityManager;
    private final JobCategoryRepository jobCategoryRepository;
    private final AreaRepository areaRepository;
    private final CompanyAreaRepository companyAreaRepository;

    public CompanyServiceImpl(UserRepository userRepository,
                              RoleRepository roleRepository,
                              UserRoleRepository userRoleRepository,
                              PasswordEncoder passwordEncoder,
                              CompanyRepository companyRepository,
                              CompanyJobCategoryRepository companyJobCategoryRepository,
                              EntityManagerFactory entityManagerFactory, EntityManager entityManager,
                              JobCategoryRepository jobCategoryRepository, AreaRepository areaRepository,
                              CompanyAreaRepository companyAreaRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
        this.passwordEncoder = passwordEncoder;
        this.companyRepository = companyRepository;
        this.companyJobCategoryRepository = companyJobCategoryRepository;
        this.entityManagerFactory = entityManagerFactory;
        this.entityManager = entityManager;
        this.jobCategoryRepository = jobCategoryRepository;
        this.areaRepository = areaRepository;
        this.companyAreaRepository = companyAreaRepository;
    }

    @Override
    public void generateCsvResponse(HttpServletResponse response) {
        String filename = "companies.csv";
        List<CompanyCSV> companyDTOS = ModelMapperUtils.mapAll(companyRepository.findAll(), CompanyCSV.class);
        CSVPrinter csvPrinter = null;
        try {
            response.setContentType("text/csv");
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + filename + "\"");
            csvPrinter = new CSVPrinter(response.getWriter(),
                    CSVFormat.DEFAULT.withHeader("ID", "Company name", "HP URL", "Contact Name", "Contact Phone", "Contact Email", "Address", "Status", "Created date"));
            for (CompanyCSV companyDTO : companyDTOS) {
                csvPrinter.printRecord(Arrays.asList(companyDTO.getId(), companyDTO.getCompanyName(), companyDTO.getHpUrl(), companyDTO.getNameContact(), companyDTO.getPhoneContact(), companyDTO.getEmailContact(), companyDTO.getAddress(), companyDTO.getStatus(), companyDTO.getCreatedDate()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (csvPrinter != null) {
                try {
                    csvPrinter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Page<CompanyShowListDto>> search(SearchCompanyRequest searchCompanyRequest, Pageable pageable) {
        try {
            final CriteriaBuilder cb = this.entityManagerFactory.getCriteriaBuilder();
            final CriteriaQuery<Company> query = cb.createQuery(Company.class);
            Object[] queryObjs = this.createQueryCompany(cb, query, searchCompanyRequest);
            Root<Company> root = (Root<Company>) queryObjs[0];
            query.select(root);
            query.where((Predicate[]) queryObjs[1]);

            TypedQuery<Company> typedQuery = this.entityManager.createQuery(query);
            typedQuery.setFirstResult((int) pageable.getOffset());
            typedQuery.setMaxResults(pageable.getPageSize());
            final List<Company> objects = typedQuery.getResultList();
            List<CompanyShowListDto> companyShowListDtos = ModelMapperUtils.mapAll(objects, CompanyShowListDto.class);

            final CriteriaBuilder cbTotal = this.entityManagerFactory.getCriteriaBuilder();
            final CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
            countQuery.select(cbTotal.count(countQuery.from(Company.class)));
            countQuery.where((Predicate[]) queryObjs[1]);
            Long total = entityManager.createQuery(countQuery).getSingleResult();
            log.info("function : Search company");
            return new ResponseEntity<>(new PageImpl<>(companyShowListDtos, pageable, total), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Object[] createQueryCompany(CriteriaBuilder cb, CriteriaQuery<?> query,
                                        SearchCompanyRequest resource) {
        final Root<Company> rootPersist = query.from(Company.class);
        final List<Predicate> predicates = new ArrayList<>();

        if (resource.getSearch() != null
                && !org.apache.commons.lang3.StringUtils.isEmpty(resource.getSearch().trim())) {
            predicates.add(cb.and(
                    cb.or(
                            cb.like(cb.upper(rootPersist.get("companyName")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("hpUrl")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("nameContact")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("phoneContact")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("emailContact")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("city")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("district")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("building")), "%" + resource.getSearch().toUpperCase() + "%"),
                            cb.like(cb.upper(rootPersist.get("room")), "%" + resource.getSearch().toUpperCase() + "%")
                    )
            ));
        }
        Object[] results = new Object[2];
        results[0] = rootPersist;
        results[1] = predicates.toArray(new Predicate[predicates.size()]);
        return results;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<CompanyShowListDto> create(
            CreateCompanyRequest createCompanyRequest) {
        try {
            //Save company
            Company company = ModelMapperUtils.map(createCompanyRequest, Company.class);
            company.setCreatedDate(new Date());
            Company saveCompany = companyRepository.save(company);

            if (createCompanyRequest.getListJobCategoryRequests() != null) {
                //Save CompanyJobCategory
                for (String listJobCategoryRequest : createCompanyRequest.getListJobCategoryRequests()) {
                    JobCategory jobCategory = jobCategoryRepository.findByCategoryName(listJobCategoryRequest);
                    CompanyJobCategory companyJobCategory = CompanyJobCategory.builder()
                            .company(saveCompany)
                            .jobCategory(jobCategory)
                            .build();
                    companyJobCategoryRepository.save(companyJobCategory);
                }
            }


            if (createCompanyRequest.getListAreaRequests() != null) {
                //Save Area with Company
                for (String listAreaRequest : createCompanyRequest.getListAreaRequests()) {
                    Area area = areaRepository.findByName(listAreaRequest);
                    CompanyArea companyArea = CompanyArea.builder()
                            .company(saveCompany)
                            .area(area)
                            .build();
                    companyAreaRepository.save(companyArea);
                }
            }

            //set data respones
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            CompanyShowListDto companyShowListDto = ModelMapperUtils.map(saveCompany, CompanyShowListDto.class);
            companyShowListDto.setAddress(saveCompany.getCity() + " " + saveCompany.getDistrict()
                    + " " + saveCompany.getBuilding() + " " + saveCompany.getRoom());
            companyShowListDto.setCreatedDate(simpleDateFormat.format(saveCompany.getCreatedDate()));
            log.info("function : create company");
            return new ResponseEntity<>(companyShowListDto, HttpStatus.OK);
        } catch (Exception e) {
            log.error("function : create company: " + e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<CompanyShowListDto> edit(Long id, UpdateCompanyRequest updateCompanyRequest) {
        try {
            Company company = companyRepository.findById(id).get();
            if (company == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
            company = setEdit(company, updateCompanyRequest);
            Company update = companyRepository.save(company);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            CompanyShowListDto companyShowListDto = ModelMapperUtils.map(update, CompanyShowListDto.class);

            companyShowListDto.setAddress(update.getCity() + " " + update.getDistrict()
                    + " " + update.getBuilding() + " " + update.getRoom());
            companyShowListDto.setCreatedDate(simpleDateFormat.format(update.getCreatedDate()));
            log.info("function : edit company");
            return new ResponseEntity<>(companyShowListDto, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public CompanyDetailDto details(Long id) {
        try {
            Company company = companyRepository.findById(id).get();
            CompanyDetailDto companyDetailDto = ModelMapperUtils.map(company, CompanyDetailDto.class);
            //set listAreaRequest in companyDetailDto
            List<CompanyArea> companyAreaList = companyAreaRepository.findAllByCompanyId(company.getId());
            List<String> listAreaa = new ArrayList<>();
            if (companyAreaList.size() != 0) {
                for (CompanyArea companyArea : companyAreaList) {
                    listAreaa.add(companyArea.getArea().getName());
                }
            }
            companyDetailDto.setListAreaRequests(listAreaa);

            //set JobCategoryRequest in companyDetailDto
            List<CompanyJobCategory> companyJobCategoryList = companyJobCategoryRepository.findAllByCompanyId(company.getId());
            List<String> listJobCategoryRequests = new ArrayList<>();
            for (CompanyJobCategory CompanyJobCategory : companyJobCategoryList) {
                listJobCategoryRequests.add(CompanyJobCategory.getJobCategory().getCategoryName());
            }
            companyDetailDto.setListJobCategoryRequests(listJobCategoryRequests);
            return companyDetailDto;
        } catch (Exception e) {
            log.info("get detail company fail", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    private Company setEdit(Company company, UpdateCompanyRequest updateCompanyRequest) {
        if (!company.getCompanyName().equals(updateCompanyRequest.getCompanyName())) {
            company.setCompanyName(updateCompanyRequest.getCompanyName());
        }
        if (!company.getCompanyNameKana().equals(updateCompanyRequest.getCompanyNameKana())) {
            company.setCompanyNameKana(updateCompanyRequest.getCompanyNameKana());
        }
        if (!company.getRegisterName().equals(updateCompanyRequest.getRegisterName())) {
            company.setRegisterName(updateCompanyRequest.getRegisterName());
        }
        if (!company.getRegisterNameKana().equals(updateCompanyRequest.getRegisterNameKana())) {
            company.setRegisterNameKana(updateCompanyRequest.getRegisterNameKana());
        }
        if (!company.getCity().equals(updateCompanyRequest.getCity())) {
            company.setCity(updateCompanyRequest.getCity());
        }
        if (!company.getDistrict().equals(updateCompanyRequest.getDistrict())) {
            company.setDistrict(updateCompanyRequest.getDistrict());
        }
        if (!company.getRoom().equals(updateCompanyRequest.getRoom())) {
            company.setRoom(updateCompanyRequest.getRoom());
        }
        if (!company.getBuilding().equals(updateCompanyRequest.getBuilding())) {
            company.setBuilding(updateCompanyRequest.getBuilding());
        }
        if (!company.getZipcode().equals(updateCompanyRequest.getZipcode())) {
            company.setZipcode(updateCompanyRequest.getZipcode());
        }
        if (!company.getHpUrl().equals(updateCompanyRequest.getHpUrl())) {
            company.setHpUrl(updateCompanyRequest.getHpUrl());
        }
        if (!company.getNameContact().equals(updateCompanyRequest.getNameContact())) {
            company.setNameContact(updateCompanyRequest.getNameContact());
        }
        if (!company.getPhoneContact().equals(updateCompanyRequest.getPhoneContact())) {
            company.setPhoneContact(updateCompanyRequest.getPhoneContact());
        }
        if (!company.getEmailContact().equals(updateCompanyRequest.getEmailContact())) {
            company.setEmailContact(updateCompanyRequest.getEmailContact());
        }
        if (!company.getStatus().equals(updateCompanyRequest.getStatus())) {
            company.setStatus(updateCompanyRequest.getStatus());
        }
        if (!company.getNote().equals(updateCompanyRequest.getNote())) {
            company.setNote(updateCompanyRequest.getNote());
        }
        if (!company.getReason().equals(updateCompanyRequest.getReason())) {
            company.setReason(updateCompanyRequest.getReason());
        }
        return company;
    }

    @Override
    public Page<UserDTO> getUserStore(Pageable pageable) {
        try {
            Page<UserRole> userPage = userRoleRepository.findAllByRoleRole("ROLE_COMPANY_ADMIN", pageable);
            Page<UserDTO> userDtoPage = userPage.map(userRole -> UserDTO.fromEntity(userRole.getUser(), "ROLE_COMPANY_ADMIN"));
            log.info("get the list of user stores");
            return userDtoPage;
        } catch (Exception e) {
            log.error("Get userStore : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to get the list");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDTO createUserCompany(CreateUserRequest createUserRequest) {
        User check = userRepository.findByEmail(createUserRequest.getEmail());
        if (check != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with same email already exists. Please try with another email address.");
        }
        User user = User.builder()
                .isAccountLocked(false)
                .name(createUserRequest.getName())
                .email(createUserRequest.getEmail())
                .password(passwordEncoder.encode(createUserRequest.getPassword()))
                .createdAt(new Date())
                .countFail(1)
                .companyId(createUserRequest.getCompanyId())
                .status(createUserRequest.isStatus())
                .build();
        User save = userRepository.save(user);
        Optional<Company> company = companyRepository.findById(createUserRequest.getCompanyId());
        if (!company.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not exist company");
        }
        Role role = roleRepository.findByRole("ROLE_COMPANY_ADMIN");
        UserRole userRole = UserRole.builder()
                .role(role)
                .user(save).build();
        userRoleRepository.save(userRole);
        log.info("Function: Create new user");
        return UserDTO.fromEntity(user, role.getRole());
    }

    @Override
    public UserDTO updateUserCompany(String email, UpdateUserRequest updateUserRequest) {
        User check = userRepository.findByEmail(email);
        if (check == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        check.setPassword(passwordEncoder.encode(updateUserRequest.getPassword()));
        check.setName(updateUserRequest.getName());
        check.setStatus(updateUserRequest.isStatus());
        check.setAccountLocked(updateUserRequest.isStatus());
        User save = userRepository.save(check);
        return UserDTO.fromEntity(save, "ROLE_COMPANY_ADMIN");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteUserCompany(String email) {
        User check = userRepository.findByEmail(email);
        if (check == null) {
            return false;
        }
        check.setStatus(false);
        userRepository.save(check);
        return true;
    }

    @Override
    public UserDTO detailUserCompany(String email) {
        User check = userRepository.findByEmail(email);
        if (check == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return UserDTO.fromEntity(check, "ROLE_COMPANY_ADMIN");
    }

    @Override
    public Page<UserDTO> getAllAccountCompany(Pageable pageable) {
        try {
            Page<User> users = userRepository.findAllByCompanyIdNotNull(pageable);
            Page<UserDTO> userDtos = users.map(user -> ModelMapperUtils.map(user, UserDTO.class));
            log.info("getList companyAccount success");
            return userDtos;
        } catch (Exception e) {
            log.error("getList companyAccount fail", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
