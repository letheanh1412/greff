package ntq.solution.greff.project.service.impl;

import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.Area;
import ntq.solution.greff.project.repository.AreaRepository;
import ntq.solution.greff.project.response.dto.AreaDTO;
import ntq.solution.greff.project.service.AreaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaServiceImpl implements AreaService {

    private final AreaRepository areaRepository;

    public AreaServiceImpl(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    @Override
    public List<AreaDTO> getAllArea() {
        return ModelMapperUtils.mapAll(areaRepository.findAllByLevel(0), AreaDTO.class);
    }

    @Override
    public List<AreaDTO> getCity() {
        return ModelMapperUtils.mapAll(areaRepository.findAllByLevel(1), AreaDTO.class);
    }

    @Override
    public Page<AreaDTO> getDistrict(String areaNameCity, Pageable pageable) {
        Area area = areaRepository.findByNameAndLevel(areaNameCity, 1);
        Page<Area> areaPage = areaRepository.findAllByLevelAndParentId(2, area.getId(), pageable);
        return areaPage.map(area1 -> ModelMapperUtils.map(area1, AreaDTO.class));
    }
}
