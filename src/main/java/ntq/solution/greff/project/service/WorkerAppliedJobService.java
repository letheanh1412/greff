package ntq.solution.greff.project.service;

import ntq.solution.greff.project.response.dto.AcceptRejectDTO;
import ntq.solution.greff.project.response.dto.DetailWorkerAppliedJobDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.response.dto.WorkerAppliedJobDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface WorkerAppliedJobService {

    Page<JobShowListDTO> showListJobApply(String email, Integer statusApply, Pageable pageable);

    Page<WorkerAppliedJobDTO> showListWorkerAppliedJob(Long id, Pageable pageable);

    DetailWorkerAppliedJobDTO detailWorkerAppliedJob(Long id);

    AcceptRejectDTO statusApplyJob(Integer statusApplyJob, Long id);
}
