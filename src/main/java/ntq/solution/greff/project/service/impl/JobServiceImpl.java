package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.DateTimeUtil;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.request.CreateJobRequest;
import ntq.solution.greff.project.request.JobLikeRequest;
import ntq.solution.greff.project.request.UpdateJobRequest;
import ntq.solution.greff.project.request.WorkTimeRequest;
import ntq.solution.greff.project.response.dto.JobDetailDTO;
import ntq.solution.greff.project.response.dto.JobConfirmDTO;
import ntq.solution.greff.project.response.dto.JobLikeDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
public class JobServiceImpl implements JobService {

    private final JobRepository jobRepository;
    private final OccupationRepository occupationRepository;
    private final JobCategoryRepository jobCategoryRepository;
    private final SkillRequiredRepository skillRequiredRepository;
    private final JobLikeRepository jobLikeRepository;
    private final UserRepository userRepository;
    private final FileStorageRepository fileStorageRepository;

    public JobServiceImpl(JobRepository jobRepository,
                          OccupationRepository occupationRepository,
                          JobCategoryRepository jobCategoryRepository,
                          SkillRequiredRepository skillRequiredRepository,
                          JobLikeRepository jobLikeRepository,
                          UserRepository userRepository,
                          FileStorageRepository fileStorageRepository) {
        this.jobRepository = jobRepository;
        this.occupationRepository = occupationRepository;
        this.jobCategoryRepository = jobCategoryRepository;
        this.skillRequiredRepository = skillRequiredRepository;
        this.jobLikeRepository = jobLikeRepository;
        this.userRepository = userRepository;
        this.fileStorageRepository = fileStorageRepository;
    }

    @Override
    public Page<JobShowListDTO> search(String search, String status, Pageable pageable) {
        try {
            search = search.replaceAll("\\W", "\\\\$0");
            Page<Job> jobPage = jobRepository.findByTitleOrWorkTimeAndAndRecruitmentStatus(search, status, pageable);
            Page<JobShowListDTO> jobShowListDTOPage = jobPage.map(job -> {
                JobShowListDTO jobShowListDTO = ModelMapperUtils.map(job, JobShowListDTO.class);
                DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("ja"));
                String date = df.format(job.getWorkDate());
                String deadlineToApplyDateTime = df.format(job.getWorkDate());
                String dateTime = date + " " +
                        job.getWorkingTimeStart() + " - " + job.getWorkingTimeEnd();
                for (Job job1 : jobPage) {
                    Optional<Occupation> occupation = occupationRepository.findById(job1.getOccupation().getId());
                    if (!occupation.isPresent()) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
                    }
                    jobShowListDTO.setWorkAddress(occupation.get().getWorkAddress());
                    jobShowListDTO.setAccessAddress(occupation.get().getAccessAddress());
                }
                jobShowListDTO.setWorkTime(dateTime);
                jobShowListDTO.setWorkDate(date);
                jobShowListDTO.setDeadlineToApplyDateTime(deadlineToApplyDateTime);
                return jobShowListDTO;
            });
            log.info("getList Job success");
            return jobShowListDTOPage;
        } catch (Exception e) {
            log.error("getList Job fail", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JobShowListDTO edit(Long id, UpdateJobRequest updateJobRequest) {
        try {
            //update job
            Optional<Job> job = jobRepository.findById(id);
            if (!job.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job does not exits");
            }
            Optional<Occupation> occupation = occupationRepository.findById(updateJobRequest.getOccupationId());
            if (!occupation.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
            }
            job.get().setOccupation(occupation.get());
            String date = updateJobRequest.getDeadlineToApplyDate() + " " + updateJobRequest.getDeadlineToApplyTime();
            job.get().setDeadlineToApplyDateTime(DateTimeUtil.convertFromShortString(date));
            job.get().setTitle(occupation.get().getTitle());
            job = Optional.of(setEdit(job.get(), updateJobRequest));
            Job update = jobRepository.save(job.get());
            JobShowListDTO jobShowListDTO = ModelMapperUtils.map(update, JobShowListDTO.class);
            log.info("update job success");
            return jobShowListDTO;
        } catch (Exception e) {
            log.error("Update Job fail", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Override
    public JobDetailDTO detail(Long id) {
        Optional<Job> job = jobRepository.findById(id);
        if (!job.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job does not exits");
        }
        Optional<Occupation> occupation = occupationRepository.findById(job.get().getOccupation().getId());
        if (!occupation.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
        }
        Optional<JobCategory> jobCategory = jobCategoryRepository.findById(occupation.get().getJobCategory().getId());
        if (!jobCategory.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job category does not exits");
        }
        List<SkillRequired> skillRequiredList = skillRequiredRepository.findAllByOccupationId(occupation.get().getId());
        StringBuilder skill = new StringBuilder();
        for (SkillRequired skillRequired : skillRequiredList) {
            skill.append(skillRequired.getNameSkill()).append(",");
        }
        skill = new StringBuilder(skill.toString().replaceAll(",$", ""));
        String str1 = job.get().getWorkTime().trim();
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("ja"));
        String date = df.format(job.get().getWorkDate());
        String[] arStr = str1.split("\\:");
        List<FileStorage> fileStorageList = fileStorageRepository.findAllByOccupationId(job.get().getOccupation().getId());
        String fileDownloadUri = "";
        if (fileStorageList.size() != 0) {
            fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/download/")
                    .path(fileStorageList.get(0).getName())
                    .toUriString();
        }
        Double amount = (Double.parseDouble(arStr[0]) + Double.parseDouble(arStr[1]) / 60) * job.get().getSalaryPerHours();
        return JobDetailDTO.builder()
                .descriptionOccupation(occupation.get().getDescription())
                .titleOccupation(occupation.get().getTitle())
                .bringItems(occupation.get().getBringItems())
                .jobCategory(jobCategory.get().getCategoryName())
                .skillRequired(skill.toString())
                .fileName(fileDownloadUri)
                .note(occupation.get().getNote())
                .workAddress(occupation.get().getWorkAddress())
                .breakTime(job.get().getBreakTime())
                .workDateTime(date)
                .workingTime(job.get().getWorkingTimeStart() + "-" + job.get().getWorkingTimeEnd())
                .deadlineToApplyDateTime(job.get().getDeadlineToApplyDateTime().toString())
                .amountTotalJob(amount)
                .salaryPerHours(job.get().getSalaryPerHours())
                .travelFees(job.get().getTravelFees())
                .workTime(job.get().getWorkTime())
                .build();
    }

    @Override
    public JobLikeDTO jobLike(JobLikeRequest jobLikeRequest) {
        try {
            Optional<Job> job = jobRepository.findById(jobLikeRequest.getJobId());
            if (!job.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job does not exits");
            }
            Optional<User> user = Optional.ofNullable(userRepository.findByEmail(jobLikeRequest.getEmail()));
            if (!user.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job does not exits");
            }
            if (jobLikeRequest.isStatus() && jobLikeRepository.findByJobId(jobLikeRequest.getJobId()) == null) {
                JobLike jobLike = JobLike.builder()
                        .job(job.get())
                        .user(user.get())
                        .build();
                jobLikeRepository.save(jobLike);
                JobLikeDTO jobLikeDTO = JobLikeDTO.builder()
                        .id(jobLike.getId())
                        .jobId(job.get().getId())
                        .build();
                log.info("like job success");
                return jobLikeDTO;
            }
            if (!jobLikeRequest.isStatus()) {
                JobLike jobLike = jobLikeRepository.findByJobId(jobLikeRequest.getJobId());
                jobLikeRepository.delete(jobLike);
            }
            return null;
        } catch (Exception e) {
            log.error("Like Job fail", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public Page<JobShowListDTO> showJobLike(String userId, Pageable pageable) {
        User user = userRepository.findByEmail(userId);
        Page<JobLike> jobLike = jobLikeRepository.findAllByUserEmailOrderByCreatAtDesc(user.getEmail(), pageable);
        Page<JobShowListDTO> jobShowListDTOS = jobLike.map(jobLike1 -> {
            JobShowListDTO jobShowListDTO = ModelMapperUtils.map(jobLike1.getJob(), JobShowListDTO.class);
            DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, new Locale("ja"));
            String date = df.format(jobLike1.getJob().getWorkDate());
            String deadlineToApplyDateTime = df.format(jobLike1.getJob().getWorkDate());
            String dateTime = date + " " +
                    jobLike1.getJob().getWorkingTimeStart() + " - " + jobLike1.getJob().getWorkingTimeEnd();
            for (JobLike job1 : jobLike) {
                Optional<Occupation> occupation = occupationRepository.findById(job1.getJob().getOccupation().getId());
                if (!occupation.isPresent()) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
                }
                //obShowListDTO.setTitle(occupation.get().getTitle());
                jobShowListDTO.setWorkAddress(occupation.get().getWorkAddress());
                jobShowListDTO.setAccessAddress(occupation.get().getAccessAddress());
            }
            jobShowListDTO.setWorkTime(dateTime);
            jobShowListDTO.setWorkDate(date);
            jobShowListDTO.setDeadlineToApplyDateTime(deadlineToApplyDateTime);
            return jobShowListDTO;
        });
        log.info("getList Job success");
        return jobShowListDTOS;
    }

    private Job setEdit(Job job, UpdateJobRequest updateJobRequest) throws ParseException {
        if (!job.getBreakTime().equals(updateJobRequest.getBreakTime())) {
            job.setBreakTime(updateJobRequest.getBreakTime());
        }
        if (!job.getNumberOfPeople().equals(updateJobRequest.getNumberOfPeople())) {
            job.setNumberOfPeople(updateJobRequest.getNumberOfPeople());
        }
        if (!job.getSalaryPerHours().equals(updateJobRequest.getSalaryPerHours())) {
            job.setSalaryPerHours(updateJobRequest.getSalaryPerHours());
        }
        if (!job.getTravelFees().equals(updateJobRequest.getTravelFees())) {
            job.setTravelFees(updateJobRequest.getTravelFees());
        }
        if (!job.getDeadlineToApplyDateTime().equals(updateJobRequest.getDeadlineToApplyDate())) {
            String date = updateJobRequest.getDeadlineToApplyDate() + " " + updateJobRequest.getDeadlineToApplyTime();
            job.setDeadlineToApplyDateTime(DateTimeUtil.convertFromShortString(date));
        }

        if (!job.getRecruitmentStatus().equals(updateJobRequest.getRecruitmentStatus())) {
            job.setRecruitmentStatus(updateJobRequest.getRecruitmentStatus());
        }
        if (!job.getSettingMatchingJob().equals(updateJobRequest.getSettingMatchingJob())) {
            job.setSettingMatchingJob(updateJobRequest.getSettingMatchingJob());
        }
        if (!job.getWorkDate().equals(updateJobRequest.getWorkDate())) {
            job.setWorkDate(DateTimeUtil.convertFromShortString(updateJobRequest.getWorkDate()));
        }
        if (!job.getWorkTime().equals(updateJobRequest.getWorkTime())) {
            job.setWorkTime(updateJobRequest.getWorkTime());
        }
        if (!job.getWorkingTimeStart().equals(updateJobRequest.getWorkingTimeStart())) {
            job.setWorkingTimeStart(updateJobRequest.getWorkingTimeStart());
        }
        if (!job.getWorkingTimeEnd().equals(updateJobRequest.getWorkingTimeEnd())) {
            job.setWorkingTimeEnd(updateJobRequest.getWorkingTimeEnd());
        }
        return job;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<JobConfirmDTO> create(CreateJobRequest createJobRequest) throws ParseException {

        List<JobConfirmDTO> jobConfirmDTOS = new ArrayList<>();
        for (WorkTimeRequest workTime : createJobRequest.getWorkTimeList()) {
            Job job = ModelMapperUtils.map(createJobRequest, Job.class);
            Optional<Occupation> occupation = occupationRepository.findById(createJobRequest.getOccupationId());
            if (!occupation.isPresent()) {
                log.error("Job creation failed : " + "Occupation not found");
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The submitted occupation cannot be found");
            }
            job.setDeadlineToApplyDateTime(DateTimeUtil.convertFromShortString(createJobRequest.getDeadlineToApplyDate()));
            job.setOccupation(occupation.get());
            job.setTitle(occupation.get().getTitle());
            job.setWorkingTimeEnd(workTime.getWorkingTimeEnd());
            job.setWorkingTimeStart(workTime.getWorkingTimeStart());
            job.setWorkTime(workTime.getWorkTime());
            job.setWorkDate(DateTimeUtil.convertFromShortString(workTime.getWorkDate()));
            Job saveJob = jobRepository.save(job);
            JobConfirmDTO jobConfirmDTO = ModelMapperUtils.map(saveJob, JobConfirmDTO.class);
            jobConfirmDTOS.add(jobConfirmDTO);
        }
        log.info("Create a new Job successfully");
        return jobConfirmDTOS;
    }
}

