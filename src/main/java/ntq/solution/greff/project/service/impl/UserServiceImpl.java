package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.entity.Role;
import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.entity.UserRole;
import ntq.solution.greff.project.repository.RoleRepository;
import ntq.solution.greff.project.repository.UserRepository;
import ntq.solution.greff.project.repository.UserRoleRepository;
import ntq.solution.greff.project.request.CreateUserRequest;
import ntq.solution.greff.project.request.ForgotRequest;
import ntq.solution.greff.project.response.dto.ChangePasswordDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.SendEmailService;
import ntq.solution.greff.project.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final SendEmailService sendEmailService;

    public UserServiceImpl(
            RoleRepository roleRepository,
            PasswordEncoder passwordEncoder,
            UserRepository userRepository,
            UserRoleRepository userRoleRepository,
            SendEmailService sendEmailService) {
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.sendEmailService = sendEmailService;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDTO create(CreateUserRequest createUserRequest) {
        try {
            User check = userRepository.findByEmail(createUserRequest.getEmail());
            if (check != null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            User user = User.builder()
                    .name(createUserRequest.getName())
                    .email(createUserRequest.getEmail())
                    .password(passwordEncoder.encode(createUserRequest.getPassword()))
                    .createdAt(new Date())
                    .countFail(0)
                    .status(true)
                    .build();
            User save = userRepository.save(user);
            Role role = roleRepository.findByRole("ROLE_BPO");
            UserRole userRole = UserRole.builder()
                    .role(role)
                    .user(save)
                    .build();
            userRoleRepository.save(userRole);
            log.info("Function : Create a new user");
            return UserDTO.fromEntity(user, role.getRole());
        } catch (Exception e) {
            log.error("Function : Create a new user");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<UserDTO> detail(String email) {
//        User user = userRepository.findByEmail(email);
//        UserDto userDto = UserDto.fromEntity(user);
//        return new ResponseEntity<>(userDto,HttpStatus.OK);
        return null;
    }

    @Override
    public ResponseEntity<String> forgot(ForgotRequest forgotRequest) {
        //Random password
        String generatedString = RandomStringUtils.randomAlphanumeric(10);
        //forgot password
        String email = forgotRequest.getEmail();
        User user = userRepository.findByEmail(email);
        if (user != null && user.isAccountLocked()) {
            user.setPassword(new BCryptPasswordEncoder().encode(generatedString));
            userRepository.save(user);
            sendEmailService.sendEmail(user.getEmail(), "Fogot password", "Password" + generatedString);
            log.info("Function : Fortgot");
            return new ResponseEntity<>("Email sent. Please check your email to receive new password", HttpStatus.OK);
        } else {
            log.error("Function : Create a new user");
            return new ResponseEntity<>("Your login email is incorrect. Please try enter!", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public User changePassWord(ChangePasswordDTO changePasswordDTO, String email) {

        User user = userRepository.findByEmail(email);
        if (passwordEncoder.matches(changePasswordDTO.getCurrentPassword(), user.getPassword()) &&
                changePasswordDTO.getNewPassword().equals(changePasswordDTO.getConfirmPassword())) {
            user.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return userRepository.save(user);
    }
}
