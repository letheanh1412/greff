package ntq.solution.greff.project.service.impl;

import lombok.SneakyThrows;
import ntq.solution.greff.project.common.DateTimeUtil;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.request.JobRecruitingDTO;
import ntq.solution.greff.project.request.SearchByArea;
import ntq.solution.greff.project.request.SearchTopPageRequest;
import ntq.solution.greff.project.response.CityResponse;
import ntq.solution.greff.project.response.RouteResponse;
import ntq.solution.greff.project.response.StationResponse;
import ntq.solution.greff.project.response.dto.TopPageJobCategoryDTO;
import ntq.solution.greff.project.service.TopPageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Service
public class TopPageServiceImpl implements TopPageService {

    private final JobRepository jobRepository;
    private final FileStorageRepository fileStorageRepository;
    private final OccupationRepository occupationRepository;
    private final JobCategoryRepository jobCategoryRepository;
    private final OccupationRoutesRepository occupationRoutesRepository;
    private final AreaRepository areaRepository;
    private final StationRouteRepository stationRouteRepository;
    private final StationRepository stationRepository;

    public TopPageServiceImpl(JobRepository jobRepository,
                              FileStorageRepository fileStorageRepository,
                              OccupationRepository occupationRepository,
                              JobCategoryRepository jobCategoryRepository,
                              OccupationRoutesRepository occupationRoutesRepository,
                              AreaRepository areaRepository,
                              StationRouteRepository stationRouteRepository,
                              StationRepository stationRepository) {
        this.jobRepository = jobRepository;
        this.fileStorageRepository = fileStorageRepository;
        this.occupationRepository = occupationRepository;
        this.jobCategoryRepository = jobCategoryRepository;
        this.occupationRoutesRepository = occupationRoutesRepository;
        this.areaRepository = areaRepository;
        this.stationRouteRepository = stationRouteRepository;
        this.stationRepository = stationRepository;
    }

    @Override
    public List<TopPageJobCategoryDTO> getJobByCategory() {
        try {
            List<JobCategory> jobCategoryList = jobCategoryRepository.findAll();
            List<TopPageJobCategoryDTO> topPageJobCategoryDTOList = new ArrayList<>();
            for (JobCategory jobCategory : jobCategoryList) {
                long jobTotal = jobRepository.countAllByOccupation_JobCategory_Id(jobCategory.getId());
                TopPageJobCategoryDTO topPageJobCategoryDTO = ModelMapperUtils.map(jobCategory,
                        TopPageJobCategoryDTO.class);
                topPageJobCategoryDTO.setJobTotal(jobTotal);
                topPageJobCategoryDTOList.add(topPageJobCategoryDTO);
            }
            return topPageJobCategoryDTOList;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public Page<JobRecruitingDTO> searchTopPage(SearchTopPageRequest searchTopPageRequest,
                                                @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.DESC)
                                                        Pageable pageable) {
        if (searchTopPageRequest.getSearchByCategory() != null) {
            return searchTopPageCategory(searchTopPageRequest.getSearchByCategory(), pageable);
        }
        if (searchTopPageRequest.getSearchByArea() != null) {
            return searchTopPageArea(searchTopPageRequest.getSearchByArea(), pageable);
        }
        Page<Job> jobPage = jobRepository.findAll(pageable);
        return getJobRecruitingDTOS(jobPage);
    }

    @Override
    public Page<JobRecruitingDTO> searchTopPageCategory(Long[] searchByCategory, Pageable pageable) {
        Page<Job> jobPage = jobRepository.searchJobByJobCategory(searchByCategory, pageable);
        return getJobRecruitingDTOS(jobPage);
    }

    private Page<JobRecruitingDTO> getJobRecruitingDTOS(Page<Job> jobPage) {
        Page<JobRecruitingDTO> jobRecruitingDTOPage = jobPage.map(new Function<Job, JobRecruitingDTO>() {
            @SneakyThrows
            @Override
            public JobRecruitingDTO apply(Job job) {
                List<FileStorage> fileStorageList = fileStorageRepository.findAllByOccupationId(job.getOccupation().getId());
                Occupation occupation = occupationRepository.findById(job.getOccupation().getId()).get();
                String str1 = job.getWorkTime().trim();
                String[] arStr = str1.split("\\:");
                Double amount = (Double.parseDouble(arStr[0]) + Double.parseDouble(arStr[1]) / 60) * job.getSalaryPerHours();
                NumberFormat formatter = new DecimalFormat("#0.00");
                String fileDownloadUri = "";
                if (fileStorageList.size() != 0) {
                    fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                            .path("/api/download/")
                            .path(fileStorageList.get(0).getName())
                            .toUriString();
                }
                return JobRecruitingDTO.builder()
                        .id(job.getId())
                        .linkImage(fileDownloadUri)
                        .workAddress(occupation.getWorkAddress())
                        .title(occupation.getTitle())
                        .salary(formatter.format(amount))
                        .workDate(DateTimeUtil.dateToString(job.getWorkDate(), "MM-dd (E)"))
                        .workingTimeEnd(job.getWorkingTimeEnd())
                        .workingTimeStart(job.getWorkingTimeStart())
                        .build();
            }
        });
        return jobRecruitingDTOPage;
    }

    @Override
    public Page<JobRecruitingDTO> searchTopPageArea(SearchByArea searchByArea, Pageable pageable) {
        Page<Job> jobPage = jobRepository.getJobTopPage(searchByArea.getAreaId(),
                searchByArea.getStationId(),
                searchByArea.getRouteId(), pageable);
        return getJobRecruitingDTOS(jobPage);
    }

    @Override
    public long countJobOfOccupation(Occupation occupation) {
        return jobRepository.countAllByOccupation(occupation);
    }

    @Override
    public long countJobOfRoutes(Long routeId) {
        List<OccupationRoutes> occupationRoutesList = occupationRoutesRepository.findAllByRoutesId(routeId);
        long totalJobOfRoute = 0;
        for (OccupationRoutes occupationRoutes : occupationRoutesList) {
            long totalJobOfOccupation = countJobOfOccupation(occupationRoutes.getOccupation());
            totalJobOfRoute = totalJobOfRoute + totalJobOfOccupation;
        }
        return totalJobOfRoute;
    }

    @Override
    public List<CityResponse> getCityByArea(String katakanaNameArea) {
        Area area = areaRepository.findByKatakanaNameLikeAndLevel(katakanaNameArea, 0);
        List<Area> listCity = areaRepository.findAllByLevelAndParentId(1, area.getId());
        return ModelMapperUtils.mapAll(listCity, CityResponse.class);
    }

    @Override
    public List<StationResponse> getStationByCity(String katakanaNameCity) {
        Area area = areaRepository.findByKatakanaNameLikeAndLevel(katakanaNameCity, 1);
        List<Stations> stationsList = stationRepository.findAllByArea(area);
        return ModelMapperUtils.mapAll(stationsList, StationResponse.class);
    }

    @Override
    public Page<RouteResponse> getRouteByStation(String katakanaNameStation, Pageable pageable) {
        Stations stations = stationRepository.findByKatakanaNameLike(katakanaNameStation);
        Page<StationRoute> stationRoutePage = stationRouteRepository.findAllByStationsId(stations.getId(), pageable);
        return stationRoutePage.map(stationRoute -> ModelMapperUtils.map(stationRoute.getRoutes(), RouteResponse.class));
    }

    public List<JobRecruitingDTO> getListRoute(Long[] routeId) {
        List<OccupationRoutes> occupationRoutesList = occupationRoutesRepository.getAllOccupationByRoute(routeId);
        return null;
    }
}