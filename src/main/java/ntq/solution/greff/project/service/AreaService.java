package ntq.solution.greff.project.service;

import ntq.solution.greff.project.response.dto.AreaDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AreaService {
    List<AreaDTO> getAllArea();

    List<AreaDTO> getCity();

    Page<AreaDTO> getDistrict(String areaNameCity, Pageable pageable);
}
