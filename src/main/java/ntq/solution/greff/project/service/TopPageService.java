package ntq.solution.greff.project.service;

import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.request.JobRecruitingDTO;
import ntq.solution.greff.project.request.SearchByArea;
import ntq.solution.greff.project.request.SearchTopPageRequest;
import ntq.solution.greff.project.response.CityResponse;
import ntq.solution.greff.project.response.RouteResponse;
import ntq.solution.greff.project.response.StationResponse;
import ntq.solution.greff.project.response.dto.TopPageJobCategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TopPageService {

    List<TopPageJobCategoryDTO> getJobByCategory();

    Page<JobRecruitingDTO> searchTopPage(SearchTopPageRequest searchTopPageRequest, Pageable pageable);

    Page<JobRecruitingDTO> searchTopPageCategory(Long[] searchByCategory, Pageable pageable);

    Page<JobRecruitingDTO> searchTopPageArea(SearchByArea searchByArea, Pageable pageable);

    long countJobOfRoutes(Long routeId);

    long countJobOfOccupation(Occupation occupation);

    List<CityResponse> getCityByArea(String katakanaNameArea);

    List<StationResponse> getStationByCity(String katakanaNameCity);

    Page<RouteResponse> getRouteByStation(String katakanaNameStation, Pageable pageable);
}
