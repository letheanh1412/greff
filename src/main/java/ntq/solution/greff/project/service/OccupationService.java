package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.CreateOccupationRequest;
import ntq.solution.greff.project.request.UpdateOccupationRequest;
import ntq.solution.greff.project.response.dto.OccupationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public interface OccupationService {

    Page<OccupationDTO> show(Pageable pageable);

    OccupationDTO create(CreateOccupationRequest createOccupationRequest) throws Exception;

    OccupationDTO edit(Long id, UpdateOccupationRequest updateOccupationRequest) throws Exception;

    boolean delete(Long id) throws Exception;

    OccupationDTO detail(Long id);
}
