package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.request.CreateStoreRequest;
import ntq.solution.greff.project.request.CreateUserStoreRequest;
import ntq.solution.greff.project.request.EditStoreRequest;
import ntq.solution.greff.project.request.StationRequest;
import ntq.solution.greff.project.response.dto.StoreDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.StoreService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class StoreServiceImpl implements StoreService {
    private final StoreRouteRepository storeRouteRepository;
    private final StoreStationRepository storeStationRepository;
    private final RouteRepository routeRepository;
    private final StationRepository stationRepository;
    private final CompanyRepository companyRepository;
    private final StoreRepository storeRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;
    private final RoleRepository roleRepository;

    public StoreServiceImpl(StoreRouteRepository storeRouteRepository,
                            StoreStationRepository storeStationRepository,
                            RouteRepository routeRepository,
                            StationRepository stationRepository,
                            CompanyRepository companyRepository,
                            StoreRepository storeRepository, UserRepository userRepository,
                            PasswordEncoder passwordEncoder,
                            UserRoleRepository userRoleRepository,
                            RoleRepository roleRepository) {
        this.storeRouteRepository = storeRouteRepository;
        this.storeStationRepository = storeStationRepository;
        this.routeRepository = routeRepository;
        this.stationRepository = stationRepository;
        this.companyRepository = companyRepository;
        this.storeRepository = storeRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleRepository = userRoleRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public Page<StoreDTO> search(String search, Pageable pageable) {
        search = search.replaceAll("\\W", "\\\\$0");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            User user = userRepository.findByEmail(authentication.getName());
            Optional<Company> company = companyRepository.findById(user.getCompanyId());
            if (!company.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "company not exist");
            }
            Long companyId = company.get().getId();
            Page<Store> storePage = storeRepository.findBySearch(companyId, search, pageable);
            Page<StoreDTO> storeDTOS = storePage.map(store -> {
                StoreDTO storeDTO = ModelMapperUtils.map(store, StoreDTO.class);
                List<StoreStation> storeStations = storeStationRepository.findAllByStoreId(store.getId());
                List<String> listStoreStation = new ArrayList<>();
                for (StoreStation storeStation : storeStations) {
                    Optional<Stations> stations = stationRepository.findById(storeStation.getStations().getId());
                    if (!stations.isPresent()) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "stations does not exist");
                    }
                    listStoreStation.add(stations.get().getKatakanaName());
                }
                storeDTO.setStations(listStoreStation);
                return storeDTO;
            });
            log.info("getList Store success");
            return storeDTOS;
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Account company not login");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public StoreDTO create(CreateStoreRequest createStoreRequest) {
        //Check Store is exits and save Store
        Store store = ModelMapperUtils.map(createStoreRequest, Store.class);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(authentication.getName());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "user not exist");
        }
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Optional<Company> company = companyRepository.findById(user.getCompanyId());
            if (!company.isPresent()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            store.setCompany(company.get());
        }
        Store saveStore = storeRepository.save(store);
        //Save Station and router
        for (StationRequest stationRequest : createStoreRequest.getStationRequestList()) {
            //Save Station
            Stations stations = stationRepository
                    .findByNameOrKanjiNameOrKatakanaName(
                            stationRequest.getName(),
                            stationRequest.getKanjiName(),
                            stationRequest.getKatakanaName()
                    );
            if (stations == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "stations not exist");
            }
            StoreStation storeStation = StoreStation.builder()
                    .store(saveStore)
                    .stations(stations)
                    .build();
            storeStationRepository.save(storeStation);
            for (String route : stationRequest.getRouteNameList()) {
                Routes routes = routeRepository.findByName(route);
                StoreRoute storeRoute = StoreRoute.builder()
                        .routes(routes)
                        .store(saveStore)
                        .build();
                storeRouteRepository.save(storeRoute);
            }
        }
        log.info("create store success");
        return ModelMapperUtils.map(saveStore, StoreDTO.class);
    }

    @Override
    public StoreDTO edit(Long id, EditStoreRequest editStoreRequest) {
        Optional<Store> optionalStore = storeRepository.findById(id);
        if (!optionalStore.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job does not exits");
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(authentication.getName());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "user not exist");
        }
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Optional<Company> company = companyRepository.findById(user.getCompanyId());
            if (!company.isPresent()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            optionalStore.get().setCompany(company.get());
        }
        optionalStore.get().setAddress(editStoreRequest.getAddress());
        optionalStore.get().setDistrict(editStoreRequest.getDistrict());
        optionalStore.get().setEmailCurator(editStoreRequest.getEmailCurator());
        optionalStore.get().setNameCurator(editStoreRequest.getNameCurator());
        optionalStore.get().setPhoneCurator(editStoreRequest.getPhoneCurator());
        optionalStore.get().setStoreName(editStoreRequest.getStoreName());
        optionalStore.get().setStoreNameKana(editStoreRequest.getStoreNameKana());
        optionalStore.get().setStatus(editStoreRequest.isStatus());
        optionalStore.get().setHpUrl(editStoreRequest.getHpUrl());
        Store saveStore = storeRepository.save(optionalStore.get());

        //Save Station and router
        if (editStoreRequest.getStationRequestList() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "stations not exist");
        }
        for (StationRequest stationRequest : editStoreRequest.getStationRequestList()) {
            //Save Station
            Stations stations = stationRepository
                    .findByNameOrKanjiNameOrKatakanaName(
                            stationRequest.getName(),
                            stationRequest.getKanjiName(),
                            stationRequest.getKatakanaName()
                    );
            StoreStation storeStation = StoreStation.builder()
                    .store(saveStore)
                    .stations(stations)
                    .build();
            storeStationRepository.save(storeStation);
            for (String route : stationRequest.getRouteNameList()) {
                Routes routes = routeRepository.findByName(route);
                StoreRoute storeRoute = StoreRoute.builder()
                        .routes(routes)
                        .store(saveStore)
                        .build();
                storeRouteRepository.save(storeRoute);
            }
            log.info("edit store success");
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDTO createUserStore(CreateUserStoreRequest createUserStoreRequest) {
        User check = userRepository.findByEmail(createUserStoreRequest.getEmail());
        if (check != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with same email already exists. Please try with another email address.");
        }
        User user = User.builder()
                .isAccountLocked(false)
                .name(createUserStoreRequest.getName())
                .email(createUserStoreRequest.getEmail())
                .password(passwordEncoder.encode(createUserStoreRequest.getPassword()))
                .createdAt(new Date())
                .countFail(1)
                .storeId(createUserStoreRequest.getStoreId())
                .status(createUserStoreRequest.isStatus())
                .build();
        User save = userRepository.save(user);
        Optional<Store> store = storeRepository.findById(createUserStoreRequest.getStoreId());
        if (!store.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not exist store");
        }
        Role role = roleRepository.findByRole("ROLE_STORE");
        UserRole userRole = UserRole.builder()
                .role(role)
                .user(save).build();
        userRoleRepository.save(userRole);
        log.info("Function: Create new store");
        return UserDTO.fromEntity(user, role.getRole());
    }

}
