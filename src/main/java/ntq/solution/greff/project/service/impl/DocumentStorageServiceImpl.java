package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.DocumentStorageException;
import ntq.solution.greff.project.entity.DegreeWorker;
import ntq.solution.greff.project.entity.DocumentStorageProperties;
import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.entity.Worker;
import ntq.solution.greff.project.repository.DegreeWorkerRepository;
import ntq.solution.greff.project.repository.UserRepository;
import ntq.solution.greff.project.repository.WorkerRepository;
import ntq.solution.greff.project.response.dto.UploadFileDTO;
import ntq.solution.greff.project.service.DocumentStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DocumentStorageServiceImpl implements DocumentStorageService {
    private final Path fileStorageLocation;

    private final WorkerRepository workerRepository;

    private final UserRepository userRepository;

    private final DegreeWorkerRepository degreeWorkerRepository;

    @Autowired
    public DocumentStorageServiceImpl(DocumentStorageProperties fileStorageProperties,
                                      WorkerRepository workerRepository,
                                      UserRepository userRepository, DegreeWorkerRepository degreeWorkerRepository) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        this.workerRepository = workerRepository;
        this.userRepository = userRepository;
        this.degreeWorkerRepository = degreeWorkerRepository;
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new DocumentStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) throws Exception {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName);
        }
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String fileName = "";
        try {
            // Check if the file's name contains invalid characters
            if (originalFileName.contains("..")) {
                throw new DocumentStorageException("Sorry! Filename contains invalid path sequence " + originalFileName);
            }
            String fileExtension = "";
            try {
                fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
            } catch (Exception e) {
                fileExtension = "";
            }
            fileName = UUID.randomUUID().toString() + fileExtension;
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new DocumentStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public List<UploadFileDTO> uploadMultipleFiles(MultipartFile[] files) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            User user = userRepository.findByEmail(authentication.getName());
            Optional<Worker> worker = Optional.ofNullable(workerRepository.findByUserEmail(user.getEmail()));
            return Arrays
                    .asList(files)
                    .stream()
                    .map(
                            file -> {
                                String upfile = storeFile(file);
                                DegreeWorker degreeWorker = DegreeWorker.builder()
                                        .fileName(upfile)
                                        .worker(worker.get())
                                        .build();
                                degreeWorkerRepository.save(degreeWorker);
                                String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                                        .path("/api/my-page/upload")
                                        .path(upfile)
                                        .toUriString();
                                return new UploadFileDTO(upfile, fileDownloadUri, "File uploaded with success!");
                            }
                    )
                    .collect(Collectors.toList());
        }
        log.info("update worker fail");
        throw new ResponseStatusException(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "not logged in");
    }
}