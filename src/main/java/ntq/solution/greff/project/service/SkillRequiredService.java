package ntq.solution.greff.project.service;

import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.entity.SkillRequired;
import org.springframework.stereotype.Service;

@Service
public interface SkillRequiredService {
    SkillRequired saveSkill(String skillRequired, Occupation occupation);
}
