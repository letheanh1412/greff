package ntq.solution.greff.project.service.impl;

import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.StationRoute;
import ntq.solution.greff.project.entity.Stations;
import ntq.solution.greff.project.repository.StationRepository;
import ntq.solution.greff.project.repository.StationRouteRepository;
import ntq.solution.greff.project.response.dto.RouteDTO;
import ntq.solution.greff.project.response.dto.StationsDTO;
import ntq.solution.greff.project.service.StationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StationServiceImpl implements StationService {
    private final StationRouteRepository stationRouteRepository;
    private final StationRepository stationRepository;

    public StationServiceImpl(StationRouteRepository stationRouteRepository,
                              StationRepository stationRepository) {
        this.stationRouteRepository = stationRouteRepository;
        this.stationRepository = stationRepository;
    }

    @Override
    public Page<StationsDTO> getAllStation(Pageable pageable) {
        Page<Stations> stationsPage = stationRepository.findAll(pageable);
        return stationsPage.map(stations -> ModelMapperUtils.map(stations, StationsDTO.class));
    }

    @Override
    public Page<RouteDTO> getRouteByStation(String name, Pageable pageable) {
        Stations stations = stationRepository
                .findByKatakanaNameLike(name);
        Page<StationRoute> stationRoute = stationRouteRepository
                .findAllByStationsId(stations.getId(), pageable);
        return stationRoute.map(stationRoute1 -> ModelMapperUtils.map(stationRoute1.getRoutes(), RouteDTO.class));
    }
}
