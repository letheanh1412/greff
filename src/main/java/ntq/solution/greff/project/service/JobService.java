package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.CreateJobRequest;
import ntq.solution.greff.project.request.JobLikeRequest;
import ntq.solution.greff.project.request.UpdateJobRequest;
import ntq.solution.greff.project.response.dto.JobConfirmDTO;
import ntq.solution.greff.project.response.dto.JobDetailDTO;
import ntq.solution.greff.project.response.dto.JobLikeDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JobService {
    List<JobConfirmDTO> create(CreateJobRequest createJobRequest) throws Exception;

    Page<JobShowListDTO> search(String status, String search, Pageable pageable);

    JobShowListDTO edit(Long id, UpdateJobRequest updateJobRequest) throws Exception;

    JobDetailDTO detail(Long id);

    JobLikeDTO jobLike(JobLikeRequest jobLikeRequest);

    Page<JobShowListDTO> showJobLike(String id, Pageable pageable);
}
