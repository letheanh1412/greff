package ntq.solution.greff.project.service;

public interface SendEmailService {
    void sendEmail(String email, String title, String body);
}
