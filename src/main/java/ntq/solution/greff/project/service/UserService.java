package ntq.solution.greff.project.service;

import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.request.CreateUserRequest;
import ntq.solution.greff.project.request.ForgotRequest;
import ntq.solution.greff.project.response.dto.ChangePasswordDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {

    UserDTO create(CreateUserRequest createUserRequest) throws Exception;

    ResponseEntity<UserDTO> detail(String email);

    ResponseEntity<String> forgot(ForgotRequest forgotRequest);

    User changePassWord(ChangePasswordDTO changePasswordDTO, String email);
}
