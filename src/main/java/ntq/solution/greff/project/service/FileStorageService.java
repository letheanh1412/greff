package ntq.solution.greff.project.service;

import ntq.solution.greff.project.entity.FileStorage;
import ntq.solution.greff.project.entity.Occupation;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
    FileStorage storeFile(MultipartFile file, Occupation occupation);
}
