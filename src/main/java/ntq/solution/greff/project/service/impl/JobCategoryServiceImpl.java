package ntq.solution.greff.project.service.impl;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.JobCategory;
import ntq.solution.greff.project.repository.JobCategoryRepository;
import ntq.solution.greff.project.request.CreateJobCategoryRequest;
import ntq.solution.greff.project.request.UpdateJobCategoryRequest;
import ntq.solution.greff.project.response.dto.JobCategoryDto;
import ntq.solution.greff.project.service.JobCategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@Slf4j
public class JobCategoryServiceImpl implements JobCategoryService {

    private final JobCategoryRepository jobCategoryRepository;

    public JobCategoryServiceImpl(JobCategoryRepository jobCategoryRepository) {
        this.jobCategoryRepository = jobCategoryRepository;
    }

    @Override
    public Page<JobCategoryDto> search(String categoryName, Pageable pageable) {
        if (categoryName == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Don't get category name");
        }
        categoryName = categoryName.replaceAll("\\W", "\\\\$0");
        Page<JobCategory> jobCategoryPage = jobCategoryRepository.findAllByCategoryNameContaining(categoryName, pageable);
        Page<JobCategoryDto> jobCategoryDtoPage = jobCategoryPage.map(jobCategory -> {
            Optional<JobCategory> jobCategoryOptional = jobCategoryRepository.findById(jobCategory.getId());
            if (!jobCategoryOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Job Category does not exist");
            }
            return JobCategoryDto.builder()
                    .id(jobCategory.getId())
                    .categoryName(jobCategoryOptional.get().getCategoryName())
                    .description(jobCategoryOptional.get().getDescription())
                    .build();
        });
        log.info("Search job category success");
        return jobCategoryDtoPage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JobCategoryDto create(CreateJobCategoryRequest createJobCategoryRequest) {
        JobCategory jobCategory = jobCategoryRepository.findByCategoryName(createJobCategoryRequest.getCategoryName());
        if (jobCategory != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job category does not exist");
        }
        JobCategory category = ModelMapperUtils.map(createJobCategoryRequest, JobCategory.class);
        JobCategory saveJobCategory = jobCategoryRepository.save(category);
        JobCategoryDto jobCategoryDto = ModelMapperUtils.map(saveJobCategory, JobCategoryDto.class);
        log.info("Create job category success");
        return jobCategoryDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JobCategoryDto edit(Long id, UpdateJobCategoryRequest updateJobCategoryRequest) {
        Optional<JobCategory> jobCategoryOptional = jobCategoryRepository.findById(id);
        if (!jobCategoryOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job category does not exits");
        }
        jobCategoryOptional.get().setCategoryName(updateJobCategoryRequest.getCategoryName());
        jobCategoryOptional.get().setDescription(updateJobCategoryRequest.getDescription());
        JobCategory save = jobCategoryRepository.save(jobCategoryOptional.get());
        log.info("Update job category success");
        return ModelMapperUtils.map(save, JobCategoryDto.class);
    }
}
