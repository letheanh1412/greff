package ntq.solution.greff.project.service;


import ntq.solution.greff.project.response.dto.RouteDTO;
import ntq.solution.greff.project.response.dto.StationsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StationService {
    Page<StationsDTO> getAllStation(Pageable pageable);

    Page<RouteDTO> getRouteByStation(String name, Pageable pageable);
}
