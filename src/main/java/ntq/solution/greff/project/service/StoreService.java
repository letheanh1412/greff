package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.CreateStoreRequest;
import ntq.solution.greff.project.request.CreateUserStoreRequest;
import ntq.solution.greff.project.request.EditStoreRequest;
import ntq.solution.greff.project.response.dto.StoreDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StoreService {
    Page<StoreDTO> search(String search, Pageable pageable);

    StoreDTO create(CreateStoreRequest createStoreRequest);

    StoreDTO edit(Long id, EditStoreRequest editStoreRequest);

    UserDTO createUserStore(CreateUserStoreRequest createUserStoreRequest);
}
