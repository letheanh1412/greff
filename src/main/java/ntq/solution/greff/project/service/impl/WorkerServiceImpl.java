package ntq.solution.greff.project.service.impl;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.DateTimeUtil;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.Area;
import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.entity.UserJob;
import ntq.solution.greff.project.entity.Worker;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.AreaRepository;
import ntq.solution.greff.project.repository.DegreeWorkerRepository;
import ntq.solution.greff.project.repository.UserRepository;
import ntq.solution.greff.project.repository.WorkerRepository;
import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.*;
import ntq.solution.greff.project.response.dto.MyPageDTO;
import ntq.solution.greff.project.response.dto.UpdateCartDTO;
import ntq.solution.greff.project.response.dto.WorkerDTO;
import ntq.solution.greff.project.response.dto.WorkerDetailDTO;
import ntq.solution.greff.project.response.dto.WorkerShowListDTO;
import ntq.solution.greff.project.service.WorkerService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
@Slf4j
public class WorkerServiceImpl implements WorkerService {
    private final WorkerRepository workerRepository;
    private final UserRepository userRepository;
    private final AreaRepository areaRepository;
    private final PasswordEncoder passwordEncoder;
    private final JavaMailSender emailSender;
    private final JobRepository jobRepository;
    private final UserJobRepository userJobRepository;
    private final DocumentStorageServiceImpl documentStorageService;
    private final DegreeWorkerRepository degreeWorkerRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    public WorkerServiceImpl(WorkerRepository workerRepository,
                             UserRepository userRepository,
                             AreaRepository areaRepository,
                             PasswordEncoder passwordEncoder,
                             JavaMailSender emailSender,
                             JobRepository jobRepository,
                             UserJobRepository userJobRepository,
                             DocumentStorageServiceImpl documentStorageService,
                             DegreeWorkerRepository degreeWorkerRepository,
                             RoleRepository roleRepository,
                             UserRoleRepository userRoleRepository) {
        this.workerRepository = workerRepository;
        this.userRepository = userRepository;
        this.areaRepository = areaRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSender = emailSender;
        this.jobRepository = jobRepository;
        this.userJobRepository = userJobRepository;
        this.documentStorageService = documentStorageService;
        this.degreeWorkerRepository = degreeWorkerRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
    }


    @Override
    public WorkerDTO createWorker(CreateWorkerRequest createWorkerRequest) {
        User checkUser = userRepository.findByEmail(createWorkerRequest.getEmail());
        if (checkUser != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "User with same email already exists. Please try with another email address.");
        }

        Optional<Area> areaOptional = areaRepository.findById(createWorkerRequest.getAreaId());
        if (!areaOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Area does not exists");
        }
        User user = User.builder()
                .email(createWorkerRequest.getEmail())
                .password("")
                .status(false)
                .build();
        userRepository.save(user);
        Role role = roleRepository.findByRole("ROLE_WORKER");
        UserRole userRole = UserRole.builder()
                .role(role)
                .user(user).build();
        userRoleRepository.save(userRole);
        String code = RandomStringUtils.randomAlphanumeric(8);
        Worker worker = Worker.builder()
                .user(user)
                .name(createWorkerRequest.getName())
                .furiganaName(createWorkerRequest.getFuriganaName())
                .phoneNumber(createWorkerRequest.getPhoneNumber())
                .areaId(areaOptional.get())
                .createDate(new Date())
                .statusConfirmation(Worker.statusConfirmation.STATUS_WAITING_APPROVE.getValue())
                .codeComfirm(code)
                .statusDelete(true)
                .statusRegister(true)
                .build();
        Worker saveWorker = workerRepository.save(worker);

        SimpleMailMessage message = getSimpleMailMessage(createWorkerRequest, code, user, worker);

        this.emailSender.send(message);
        log.info("Worker is registered successfully");
        return ModelMapperUtils.map(saveWorker, WorkerDTO.class);
    }

    private SimpleMailMessage getSimpleMailMessage(CreateWorkerRequest createWorkerRequest, String code,
                                                   User user, Worker worker) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(createWorkerRequest.getEmail());
        message.setSubject("Greff Create Worker Message");
        message.setText("Hi " + worker.getName() + "\n" +
                "Email: " + user.getEmail() + "\n" +
                "Code: " + code + "\n" +
                "Thank you very much!");
        return message;
    }

    @Override
    public WorkerDTO sendCodeAgain(CreateWorkerRequest createWorkerRequest) {
        Worker worker = workerRepository.findByUser(userRepository.findByEmail(createWorkerRequest.getEmail()));
        String sendCode = RandomStringUtils.randomAlphanumeric(8);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(createWorkerRequest.getEmail());
        message.setSubject("Greff Create Worker Message");
        message.setText("Hi " + worker.getName() + "\n" +
                "Email: " + createWorkerRequest.getEmail() + "\n" +
                "Code: " + sendCode + "\n" +
                "Thank you very much!");
        this.emailSender.send(message);
        worker.setCodeComfirm(sendCode);
        Worker save = workerRepository.save(worker);
        return ModelMapperUtils.map(save, WorkerDTO.class);
    }

    @Override
    public MyPageDTO updateWorker(UpdateWorkerRequest updateWorkerRequest) throws ParseException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Worker worker = workerRepository.findByUserEmail(authentication.getName());
            String fileName = "";
            if (updateWorkerRequest.getFile() != null) {
                fileName = documentStorageService.storeFile(updateWorkerRequest.getFile());
                worker.setAvatar(fileName);
            }
            setUpdate(worker, updateWorkerRequest);
            Worker update = workerRepository.save(worker);
            MyPageDTO myPageDTO = ModelMapperUtils.map(update, MyPageDTO.class);
            myPageDTO.setDateOfBirth(update.getBirthday().toString());
            myPageDTO.setAvatar(getFileURL(fileName));
            log.info("update worker success");
            return myPageDTO;
        }
        log.info("update worker fail");
        throw new ResponseStatusException(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "not logged in");
    }

    @Override
    public MyPageDTO infoWorker() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Worker worker = workerRepository.findByUser(userRepository.findByEmail(authentication.getName()));
            MyPageDTO myPageDTO = ModelMapperUtils.map(worker, MyPageDTO.class);
            myPageDTO.setAvatar(getFileURL(worker.getAvatar()));
            String name = worker.getName().trim();
            String furigaraName = worker.getFuriganaName().trim();
            String[] arStr = name.split("\\ ");
            if (arStr.length == 2) {
                myPageDTO.setFirstName(arStr[0]);
                myPageDTO.setLastName(arStr[1]);
            }
            String[] furigara = furigaraName.split("\\ ");
            if (furigara.length == 2) {
                myPageDTO.setFirstFuriganaName(furigara[0]);
                myPageDTO.setLastFuriganaName(furigara[1]);
            }
            myPageDTO.setEmail(authentication.getName());
            log.info("get info worker success");
            return myPageDTO;
        }
        log.info("get info worker fail");
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private String getFileURL(String fileName) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(fileName)
                .toUriString();
    }

    @Override
    public UpdateCartDTO updateCard(UpdateCardRequest updateCardRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Worker worker = workerRepository.findByUserEmail(authentication.getName());
            String fileNameAfterCart = documentStorageService.storeFile(updateCardRequest.getAfterOfIdentityCard());
            String fileNameFrontCart = documentStorageService.storeFile(updateCardRequest.getAfterOfIdentityCard());
            worker.setAfterOfIdentityCard(fileNameAfterCart);
            worker.setFrontOfIdentityCard(fileNameFrontCart);
            workerRepository.save(worker);
            return UpdateCartDTO.builder()
                    .afterOfIdentityCard(getFileURL(fileNameAfterCart))
                    .frontOfIdentityCard(getFileURL(fileNameFrontCart))
                    .build();
        }
        log.info("update worker fail");
        throw new ResponseStatusException(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "not logged in");
    }

    @Override
    public ApproveRejectDTO StatusConfirmation(Integer StatusConfirmation, Long idWorker) {
        try {
            Optional<Worker> worker = workerRepository.findById(idWorker);
            if (!worker.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Worker does not exits");
            }
            worker.get().setStatusConfirmation(StatusConfirmation);
            workerRepository.save(worker.get());
            ApproveRejectDTO approveRejectDTO = ApproveRejectDTO.builder()
                    .id(idWorker)
                    .status(StatusConfirmation)
                    .build();
            log.info("Change StatusConfirmation success");
            return approveRejectDTO;
        } catch (Exception e) {
            log.info("Change StatusConfirmation fail");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ApplyJobDTO applyJob(ApplyJobRequest applyJobRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Worker worker = workerRepository.findByUserEmail(authentication.getName());
            if (worker == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Worker not exist");
            }
            if (worker.getStatusConfirmation() == Worker.statusConfirmation.STATUS_APPROVE.getValue()) {
                Optional<Job> job = jobRepository.findById(applyJobRequest.getJobId());
                if (!job.isPresent()) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "job not exist");
                }
                List<UserJob> userJobs = userJobRepository.findAll();
                for (UserJob userJob : userJobs) {
                    if (applyJobRequest.getJobId().equals(userJob.getJob().getId())
                            && worker.getId().equals(userJob.getWorker().getId())) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Have applied");
                    }
                }
                UserJob userJob = UserJob.builder()
                        .email(applyJobRequest.getEmail())
                        .furiganaName(applyJobRequest.getFuriganaName())
                        .job(job.get())
                        .name(applyJobRequest.getName())
                        .phoneNumber((applyJobRequest.getPhoneNumber()))
                        .worker(worker)
                        .statusApply(UserJob.statusApply.STATUS_NEW_REQUEST.getValue())
                        .build();
                userJobRepository.save(userJob);
                ApplyJobDTO applyJobDTO = ApplyJobDTO.builder()
                        .email(applyJobRequest.getEmail())
                        .email(applyJobRequest.getEmail())
                        .furiganaName(applyJobRequest.getFuriganaName())
                        .name(applyJobRequest.getName())
                        .phoneNumber((applyJobRequest.getPhoneNumber()))
                        .build();
                log.info("apply job success");
                return applyJobDTO;
            }
        }
        log.error("apply job fail");
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private Worker setUpdate(Worker worker, UpdateWorkerRequest updateWorkerRequest) throws ParseException {
        Date birthdayUpdate = null;
        if (updateWorkerRequest.getBirthday() != null) {
            birthdayUpdate = DateTimeUtil.convertFromShortString(updateWorkerRequest.getBirthday());
        }
        if (updateWorkerRequest.getName() != null) {
            if (!worker.getName().equals(updateWorkerRequest.getName())) {
                worker.setName(updateWorkerRequest.getName());
            }
        }
        if (updateWorkerRequest.getFuriganaName() != null) {
            if (!worker.getFuriganaName().equals(updateWorkerRequest.getFuriganaName())) {
                worker.setFuriganaName(updateWorkerRequest.getFuriganaName());
            }
        }
        if (worker.getGender() == null) {
            worker.setGender(updateWorkerRequest.getGender());
        }
        if (!worker.getGender().equals(updateWorkerRequest.getGender())) {
            worker.setGender(updateWorkerRequest.getGender());
        }
        if (worker.getBirthday() == null) {
            worker.setBirthday(birthdayUpdate);
        }
        if (!worker.getBirthday().equals(birthdayUpdate)) {
            worker.setBirthday(birthdayUpdate);
        }
        if (worker.getPhoneNumber() == null) {
            worker.setBirthday(birthdayUpdate);
        }
        if (!worker.getPhoneNumber().equals(updateWorkerRequest.getPhoneNumber())) {
            worker.setPhoneNumber(updateWorkerRequest.getPhoneNumber());
        }
        return worker;
    }

    @Override
    public WorkerDTO checkCodeConfirm(Long id, CheckCodeRequest checkCodeRequest) {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The worker ID doesn't match");
        }
        if (checkCodeRequest.getCodeConfirm() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "The desired work area does not exist");
        }
        if (!workerOptional.get().getCodeComfirm().equals(checkCodeRequest.getCodeConfirm())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Code is incorrect");
        }
        log.info("Correct code");
        User user = userRepository.findByEmail(workerOptional.get().getUser().getEmail());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        workerOptional.get().setStatusRegister(false);
        workerRepository.save(workerOptional.get());
        user.setStatus(true);
        User save = userRepository.save(user);
        return ModelMapperUtils.map(save, WorkerDTO.class);
    }

    @Override
    public WorkerDTO setPassword(Long id, UpdatePassword updatePassword) {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The worker ID you want to change does not exist");
        }
        if (workerOptional.get().getStatusRegister()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Please confirm the code before setting the password");
        }
        if (updatePassword.getPassword() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Password cannot be blank");
        }
        User user = userRepository.findByEmail(workerOptional.get().getUser().getEmail());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        user.setPassword(passwordEncoder.encode(updatePassword.getPassword()));
        workerOptional.get().setCodeComfirm(null);
        workerRepository.save(workerOptional.get());
        User save = userRepository.save(user);
        log.info("Password has been set successfully");
        return ModelMapperUtils.map(save, WorkerDTO.class);
    }

    @Override
    public WorkerDTO getMailMessage(ForgotPasswordWorkerRequest forgotPasswordWorkerRequest) {
        if (forgotPasswordWorkerRequest.getEmail() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Have not received the email, dear ");
        }
        User user = userRepository.findByEmail(forgotPasswordWorkerRequest.getEmail());
        Worker worker = workerRepository.findByUser(user);
        if (worker == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Worker does not exist");
        }
        String code = RandomStringUtils.randomAlphanumeric(8);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(forgotPasswordWorkerRequest.getEmail());
        message.setSubject("Greff Create Worker Message");
        message.setText("Hi " + worker.getName() + "\n" +
                "Email: " + forgotPasswordWorkerRequest.getEmail() + "\n" +
                "Code: " + code);
        this.emailSender.send(message);
        worker.setCodeComfirm(code);
        workerRepository.save(worker);
        return ModelMapperUtils.map(worker, WorkerDTO.class);
    }

    @Override
    public Page<WorkerShowListDTO> searchWorker(String search, String startDate, String endDate,
                                                @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.DESC)
                                                        Pageable pageable) {
        Page<Worker> workerPage = workerRepository.getAllWorkers(search, startDate, endDate, pageable);
        return getWorkerDTOS(workerPage);
    }

    private Page<WorkerShowListDTO> getWorkerDTOS(Page<Worker> workerPage) {

        Page<WorkerShowListDTO> workerShowListDTOPage = workerPage.map(new Function<Worker, WorkerShowListDTO>() {
            @SneakyThrows
            @Override
            public WorkerShowListDTO apply(Worker worker) {
                Optional<Worker> workerOptional = workerRepository.findById(worker.getId());
                if (!workerOptional.isPresent()) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                }

                User user;
                if (workerOptional.get().getUser() != null) {
                    Optional<User> userOptional = userRepository.findById(workerOptional.get().getUser().getEmail());
                    if (!userOptional.isPresent()) {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                    }
                    user = userOptional.get();
                } else {
                    user = new User();
                }

                List<Worker> workerList = workerRepository.getAllWorkersTotal();
                List<Worker> workerList1 = workerRepository.getAllWorkersToDay();

                return WorkerShowListDTO.builder()
                        .id(workerOptional.get().getId())
                        .name(workerOptional.get().getName())
                        .email(user.getEmail())
                        .phone(workerOptional.get().getPhoneNumber())
                        .status(user.getStatus())
                        .createdDate(DateTimeUtil.dateToString(workerOptional.get().getCreateDate(), "yyyy-MM-dd HH:mm"))
                        .workerTotal(workerList.size())
                        .workerTotalInToDay(workerList1.size())
                        .build();
            }
        });
        log.info("Search Worker was successful");
        return workerShowListDTOPage;
    }

    @Override
    public boolean deleteWorker(Long id) {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Worker wanted to delete does not exist");
        }
        workerOptional.get().setStatusDelete(false);
        workerRepository.save(workerOptional.get());
        log.info("Successfully deleted the worker");
        return true;
    }

    @Override
    public WorkerDetailDTO detailWorker(Long id) {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The worker you want to see detail does not exist");
        }
        Optional<User> userOptional = userRepository.findById(workerOptional.get().getUser().getEmail());
        if (!userOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Email user does not exist");
        }
        Optional<Area> areaOptional = areaRepository.findById(workerOptional.get().getAreaId().getId());
        if (!areaOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Work area does not exist");
        }
        List<String> fileNameURLCertificate = getStrings(workerOptional);

        String fileDownloadUriFrontOfIdentityCard = getString(workerOptional);

        String fileDownloadUriAfterOfIdentityCard = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(workerOptional.get().getAfterOfIdentityCard())
                .toUriString();

        String fileDownloadUriAvatar = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(workerOptional.get().getAvatar())
                .toUriString();

        WorkerDetailDTO workerDetailDTO = WorkerDetailDTO.builder()
                .avatar(fileDownloadUriAvatar)
                .name(workerOptional.get().getName())
                .furiganaName(workerOptional.get().getFuriganaName())
                .gender(workerOptional.get().getGender())
                .birthday(workerOptional.get().getBirthday())
                .areaWantToWork(areaOptional.get().getName())
                .email(userOptional.get().getEmail())
                .phoneNumber(workerOptional.get().getPhoneNumber())
                .certificate(fileNameURLCertificate)
                .frontOfIdentityCard(fileDownloadUriFrontOfIdentityCard)
                .afterOfIdentityCard(fileDownloadUriAfterOfIdentityCard)
                .build();
        log.info("View details of Successful Workers");
        return workerDetailDTO;
    }

    private String getString(Optional<Worker> workerOptional) {
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Worker does not exits");
        }
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(workerOptional.get().getFrontOfIdentityCard())
                .toUriString();
    }

    private List<String> getStrings(Optional<Worker> workerOptional) {
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Worker does not exits");
        }
        List<String> fileNameURLCertificate = new ArrayList<>();
        List<DegreeWorker> degreeWorkerList = degreeWorkerRepository.findAllByWorkerId(workerOptional.get().getId());
        for (DegreeWorker item : degreeWorkerList) {
            String fileDownloadUriCertificate = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/download/")
                    .path(item.getFileName())
                    .toUriString();
            fileNameURLCertificate.add(fileDownloadUriCertificate);
        }
        return fileNameURLCertificate;
    }

    @Override
    public WorkerDetailDTO editWorker(Long id, EditWorkerRequest editWorkerRequest) throws ParseException {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        if (!workerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The Worker id you want to edit does not exist");
        }
        Optional<Area> editArea = areaRepository.findById(editWorkerRequest.getAreaWantToWork());
        if (!editArea.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "This work area does not exist");
        }
        workerOptional.get().setName(editWorkerRequest.getName());
        workerOptional.get().setFuriganaName(editWorkerRequest.getFuriganaName());
        workerOptional.get().setPhoneNumber(editWorkerRequest.getPhoneNumber());
        workerOptional.get().setGender(editWorkerRequest.getGender());
        workerOptional.get().setBirthday(DateTimeUtil.convertFromShortString(editWorkerRequest.getBirthday()));
        workerOptional.get().setAreaId(editArea.get());
        Worker save = workerRepository.save(workerOptional.get());
        log.info("Successfully edited worker information");
        return ModelMapperUtils.map(save, WorkerDetailDTO.class);
    }

    @Override
    public WorkerDetailDTO createWorkerByAdmin(CreateWorkerByAdmin createWorkerByAdmin) {

        User checkUser = userRepository.findByEmail(createWorkerByAdmin.getEmail());
        if (checkUser != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "This email already has an account, please use another email");
        }

        Optional<Area> areaOptional = areaRepository.findById(createWorkerByAdmin.getAreaId());
        if (!areaOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The desired work area does not exist");
        }
        User user = User.builder()
                .email(createWorkerByAdmin.getEmail())
                .password(passwordEncoder.encode("123456"))
                .status(false)
                .build();
        userRepository.save(user);

        String code = RandomStringUtils.randomAlphanumeric(8);
        Worker worker = Worker.builder()
                .user(user)
                .name(createWorkerByAdmin.getName())
                .furiganaName(createWorkerByAdmin.getFuriganaName())
                .phoneNumber(createWorkerByAdmin.getPhoneNumber())
                .areaId(areaOptional.get())
                .createDate(new Date())
                .codeComfirm(code)
                .statusDelete(true)
                .statusRegister(true)
                .build();
        Worker saveWorker = workerRepository.save(worker);
        SimpleMailMessage message = getSimpleMailMessageAdmin(createWorkerByAdmin, code, user, worker);
        this.emailSender.send(message);
        log.info("The account has been created successfully");
        return ModelMapperUtils.map(saveWorker, WorkerDetailDTO.class);

    }

    private SimpleMailMessage getSimpleMailMessageAdmin(CreateWorkerByAdmin createWorkerByAdmin, String code,
                                                        User user, Worker worker) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(createWorkerByAdmin.getEmail());
        message.setSubject("Greff Create Worker Message");
        message.setText("Hi " + worker.getName() + "\n" +
                "Email: " + user.getEmail() + "\n" +
                "Code: " + code + "\n" +
                "Use the code to set up the password so you can use the account" + "\n" +
                "Thank you very much!");
        return message;
    }
}
