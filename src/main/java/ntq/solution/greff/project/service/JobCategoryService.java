package ntq.solution.greff.project.service;

import ntq.solution.greff.project.request.CreateJobCategoryRequest;
import ntq.solution.greff.project.request.UpdateJobCategoryRequest;
import ntq.solution.greff.project.response.dto.JobCategoryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JobCategoryService {
    Page<JobCategoryDto> search(String categoryName, Pageable pageable);

    JobCategoryDto create(CreateJobCategoryRequest createJobCategoryRequest) throws Exception;

    JobCategoryDto edit(Long id, UpdateJobCategoryRequest updateJobCategoryRequest) throws Exception;

}