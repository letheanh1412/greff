package ntq.solution.greff.project.service.impl;


import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.common.ModelMapperUtils;
import ntq.solution.greff.project.entity.*;
import ntq.solution.greff.project.repository.*;
import ntq.solution.greff.project.request.CreateOccupationRequest;
import ntq.solution.greff.project.request.UpdateOccupationRequest;
import ntq.solution.greff.project.response.dto.*;
import ntq.solution.greff.project.service.OccupationService;
import ntq.solution.greff.project.service.SkillRequiredService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class OccupationServiceImpl implements OccupationService {
    private final OccupationRepository occupationRepository;
    private final SkillRequiredService skillRequiredService;
    private final SkillRequiredRepository skillRequiredRepository;
    private final SpecialityRepository specialityRepository;
    private final JobCategoryRepository jobCategoryRepository;
    private final FileStorageRepository fileStorageRepository;
    private final JobRepository jobRepository;
    private final RouteRepository routeRepository;
    private final DocumentStorageServiceImpl documentStorageService;

    public OccupationServiceImpl(OccupationRepository occupationRepository,
                                 SkillRequiredService skillRequiredService,
                                 SkillRequiredRepository skillRequiredRepository,
                                 SpecialityRepository specialityRepository,
                                 JobCategoryRepository jobCategoryRepository,
                                 FileStorageRepository fileStorageRepository,
                                 JobRepository jobRepository,
                                 RouteRepository routeRepository,
                                 DocumentStorageServiceImpl documentStorageService) {
        this.occupationRepository = occupationRepository;
        this.skillRequiredService = skillRequiredService;
        this.skillRequiredRepository = skillRequiredRepository;
        this.specialityRepository = specialityRepository;
        this.jobCategoryRepository = jobCategoryRepository;
        this.fileStorageRepository = fileStorageRepository;
        this.jobRepository = jobRepository;
        this.routeRepository = routeRepository;
        this.documentStorageService = documentStorageService;
    }

    @Override
    public Page<OccupationDTO> show(Pageable pageable) {
        Page<Occupation> occupationPage = occupationRepository.findAll(pageable);
        return occupationPage.map(occupation -> {
            List<String> fileNameURL = new ArrayList<>();
            List<FileStorage> fileStorages = fileStorageRepository.findAllByOccupationId(occupation.getId());
            for (FileStorage item : fileStorages) {
                String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/api/download/")
                        .path(item.getName())
                        .toUriString();
                fileNameURL.add(fileDownloadUri);
            }
            OccupationDTO occupationDTO = ModelMapperUtils.map(occupation, OccupationDTO.class);
            occupationDTO.setFiles(fileNameURL);
            return occupationDTO;
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public OccupationDTO create(CreateOccupationRequest createOccupationRequest) {
        Occupation occupation = ModelMapperUtils.map(createOccupationRequest, Occupation.class);
        occupation.setId(null);
        Optional<JobCategory> jobCategory = jobCategoryRepository.findById(createOccupationRequest.getCategoryId());
        if (!jobCategory.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job category does exits");
        }
        occupation.setJobCategory(jobCategory.get());
        Occupation saveOccupation = occupationRepository.save(occupation);
        List<String> fileNameURL = getStrings(createOccupationRequest, occupation);
        List<Speciality> specialityArrayList = getSpecialities(createOccupationRequest, occupation);
        List<SkillRequired> skillRequiredArrayList = getSkillRequireds(createOccupationRequest, saveOccupation);

        OccupationDTO occupationDTO = getOccupationDTO(saveOccupation, fileNameURL, specialityArrayList,
                skillRequiredArrayList, createOccupationRequest.getRouteId());
        log.info("Create occupation success");
        return occupationDTO;
    }

    private OccupationDTO getOccupationDTO(Occupation saveOccupation,
                                           List<String> fileNameURL,
                                           List<Speciality> specialityArrayList,
                                           List<SkillRequired> skillRequiredArrayList,
                                           List<Long> routeId) {
        OccupationDTO occupationDTO = ModelMapperUtils.map(saveOccupation, OccupationDTO.class);
        occupationDTO.setFiles(fileNameURL);
        occupationDTO.setSpecialityDTOS(ModelMapperUtils.mapAll(specialityArrayList, SpecialityDTO.class));
        occupationDTO.setSkillRequiredDTOS(ModelMapperUtils.mapAll(skillRequiredArrayList, SkillRequiredDTO.class));
        occupationDTO.setRouteDTOS(ModelMapperUtils.mapAll(routeRepository.findAllById(routeId), RouteDTO.class));
        return occupationDTO;
    }

    private List<SkillRequired> getSkillRequireds(CreateOccupationRequest createOccupationRequest, Occupation saveOccupation) {
        List<SkillRequired> skillRequiredArrayList = new ArrayList<>();
        if (createOccupationRequest.getSkillRequiredList() != null) {
            for (String nameSkill : createOccupationRequest.getSkillRequiredList()) {
                SkillRequired skillRequired = skillRequiredService.saveSkill(nameSkill, saveOccupation);
                skillRequiredArrayList.add(skillRequired);
            }
        }
        return skillRequiredArrayList;
    }

    private List<Speciality> getSpecialities(CreateOccupationRequest createOccupationRequest, Occupation occupation) {
        List<Speciality> specialityArrayList = new ArrayList<>();
        if (createOccupationRequest.getSpecialityList() != null) {
            for (String content : createOccupationRequest.getSpecialityList()) {
                Speciality speciality = Speciality.builder()
                        .content(content)
                        .occupation(occupation)
                        .build();
                specialityRepository.save(speciality);
                specialityArrayList.add(speciality);
            }
        }
        return specialityArrayList;
    }

    private List<String> getStrings(CreateOccupationRequest createOccupationRequest, Occupation occupation) {
        List<String> fileNameURL = new ArrayList<>();
        if (createOccupationRequest.getPhotos() != null) {
            String fileName = documentStorageService.storeFile(createOccupationRequest.getPhotos());
            FileStorage fileStorage = FileStorage.builder()
                    .name(fileName)
                    .occupation(occupation)
                    .build();
            fileStorageRepository.save(fileStorage);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/download/")
                    .path(fileName)
                    .toUriString();
            fileNameURL.add(fileDownloadUri);
        }
        return fileNameURL;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public OccupationDTO edit(Long id, UpdateOccupationRequest updateOccupationRequest) {
        Occupation occupation = ModelMapperUtils.map(updateOccupationRequest, Occupation.class);
        occupation.setId(id);
        Optional<JobCategory> jobCategory = jobCategoryRepository.findById(updateOccupationRequest.getCategoryId());
        if (!jobCategory.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Job category does not exits");
        }
        Optional<Occupation> occupationOptional = occupationRepository.findById(id);
        if (!occupationOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
        }
        occupationOptional.get().setJobCategory(jobCategory.get());
        occupationOptional.get().setTitle(updateOccupationRequest.getTitle());
        occupationOptional.get().setDescription(updateOccupationRequest.getDescription());
        occupationOptional.get().setWorkAddress(updateOccupationRequest.getWorkAddress());
        occupationOptional.get().setAccessAddress(updateOccupationRequest.getAccessAddress());
        occupationOptional.get().setBringItems(updateOccupationRequest.getBringItems());
        occupationOptional.get().setStatus(updateOccupationRequest.isStatus());
        Occupation save = occupationRepository.save(occupationOptional.get());

        List<String> fileNameURL = getStrings(updateOccupationRequest, save);

        List<SkillRequired> skillRequiredArrayList = getSkillRequireds(updateOccupationRequest, save);

        List<Speciality> specialityArrayList = getSpecialities(updateOccupationRequest, save);

        OccupationDTO occupationDTO = getOccupationDTO(occupationOptional.get(),
                fileNameURL, specialityArrayList, skillRequiredArrayList,
                updateOccupationRequest.getRouteId());
        log.info("Update occupation success");
        return occupationDTO;
    }

    private List<Speciality> getSpecialities(UpdateOccupationRequest updateOccupationRequest, Occupation save) {
        List<Speciality> specialityArrayList = specialityRepository.findAllByOccupation(save);
        for (Speciality speciality : specialityArrayList) {
            specialityRepository.deleteById(speciality.getId());
        }
        for (String content : updateOccupationRequest.getSpecialityList()) {
            Speciality speciality = Speciality.builder()
                    .content(content)
                    .occupation(save)
                    .build();
            specialityRepository.save(speciality);
        }
        return specialityArrayList;
    }

    private List<SkillRequired> getSkillRequireds(UpdateOccupationRequest updateOccupationRequest, Occupation save) {
        List<SkillRequired> skillRequiredArrayList = skillRequiredRepository.findAllByOccupation(save);
        for (SkillRequired skill : skillRequiredArrayList) {
            skillRequiredRepository.deleteById(skill.getId());
        }
        for (String nameSkill : updateOccupationRequest.getSkillRequiredList()) {
            SkillRequired requireds = SkillRequired.builder()
                    .nameSkill(nameSkill)
                    .occupation(save)
                    .build();
            skillRequiredRepository.save(requireds);
        }
        return skillRequiredArrayList;
    }

    private List<String> getStrings(UpdateOccupationRequest updateOccupationRequest, Occupation save) {
        List<FileStorage> fileStorageList = fileStorageRepository.findAllByOccupation(save);
        for (FileStorage name : fileStorageList) {
            fileStorageRepository.deleteById(name.getId());
        }
        List<String> fileNameURL = new ArrayList<>();
        String fileName = "";
        if (updateOccupationRequest.getPhotos() != null) {
            fileName = documentStorageService.storeFile(updateOccupationRequest.getPhotos());
        }
        FileStorage fileStorage = FileStorage.builder()
                .name(fileName)
                .occupation(save)
                .build();
        fileStorageRepository.save(fileStorage);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/download/")
                .path(fileName)
                .toUriString();
        fileNameURL.add(fileDownloadUri);
        return fileNameURL;
    }

    @Override
    public boolean delete(Long id) {
        try {
            Optional<Occupation> occupationOptional = occupationRepository.findById(id);
            if (!occupationOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Occupation does not exits");
            }
            long countJob = jobRepository.countAllByOccupation(occupationOptional.get());
            if (countJob > 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Không thể xóa vì đang có Job tồn tại");
            }
            occupationOptional.get().setStatus(false);
            occupationRepository.save(occupationOptional.get());
            return true;
        } catch (Exception e) {
            log.error("Xóa occupation không thành công : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Lỗi hệ thống");
        }
    }

    @Override
    public OccupationDTO detail(Long id) {
        try {
            Optional<Occupation> occupationOptional = occupationRepository.findById(id);
            if (!occupationOptional.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Occupation does not exits");
            }
            OccupationDTO occupationDTO = ModelMapperUtils.map(occupationOptional.get(), OccupationDTO.class);
            List<String> fileNameURL = new ArrayList<>();
            List<FileStorage> fileStorageList = fileStorageRepository.findAllByOccupationId(occupationOptional.get().getId());
            for (FileStorage item : fileStorageList) {
                String fileDownloadUriCertificate = ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/api/download/")
                        .path(item.getName())
                        .toUriString();
                fileNameURL.add(fileDownloadUriCertificate);
            }
            List<SkillRequiredDTO> skillRequiredsList = ModelMapperUtils.mapAll(
                    skillRequiredRepository.findAllByOccupation(occupationOptional.get()),
                    SkillRequiredDTO.class
            );
            List<SpecialityDTO> specialityDTOList = ModelMapperUtils.mapAll(
                    specialityRepository.findAllByOccupation(occupationOptional.get()),
                    SpecialityDTO.class
            );
            occupationDTO.setSkillRequiredDTOS(skillRequiredsList);
            occupationDTO.setSpecialityDTOS(specialityDTOList);
            occupationDTO.setFiles(fileNameURL);
            log.info("Xem chi tiết occupation thành công");
            return occupationDTO;
        } catch (Exception e) {
            log.error("Xem chi tiết occupation thất bại : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
