package ntq.solution.greff.project.request;

import lombok.Data;

import java.util.List;

@Data
public class StationRequest {
    private String name;
    private String katakanaName;
    private String kanjiName;
    private List<String> routeNameList;
}
