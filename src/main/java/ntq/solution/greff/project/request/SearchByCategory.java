package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchByCategory {
    private Long[] categoryId;
}
