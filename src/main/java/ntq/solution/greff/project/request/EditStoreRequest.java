package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class EditStoreRequest {
    @NotEmpty
    @NotBlank
    @NotNull(message = "storeName not null")
    private String storeName;
    private String storeNameKana;
    private String district;
    private String address;
    private String hpUrl;
    @NotEmpty
    @NotBlank
    @NotNull(message = "emailCurator not null")
    private String emailCurator;
    @NotEmpty
    @NotBlank
    @NotNull(message = "nameCurator not null")
    private String nameCurator;
    @NotEmpty
    @NotBlank
    @NotNull(message = "phoneCurator not null")
    private String phoneCurator;
    private boolean status;
    private Long companyId;
    private List<StationRequest> stationRequestList;
}
