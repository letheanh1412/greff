package ntq.solution.greff.project.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ListOccupationRequest {
    private String title;
    private String description;
    private MultipartFile photos;
}
