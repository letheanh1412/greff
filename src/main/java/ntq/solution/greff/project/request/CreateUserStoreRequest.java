package ntq.solution.greff.project.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class CreateUserStoreRequest {
    @NotEmpty
    @NotBlank
    @NotNull(message = "Name not null")
    private String name;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Email not null")
    private String email;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Password not null")
    private String password;
    private boolean status;
    private Long storeId;
}
