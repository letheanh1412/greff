package ntq.solution.greff.project.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EditWorkerRequest {
    private String name;
    private String furiganaName;
    private Long areaWantToWork;
    private String phoneNumber;
    private String gender;
    private String birthday;
}
