package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchJobCategoryRequest {
    private String categoryName;
}
