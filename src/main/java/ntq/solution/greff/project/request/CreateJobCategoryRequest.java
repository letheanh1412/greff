package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CreateJobCategoryRequest {
    @NotEmpty
    @NotNull
    @NotBlank(message = "categoryName not null")
    private String categoryName;
    @NotEmpty
    @NotNull
    @NotBlank(message = "description not null")
    private String description;
}
