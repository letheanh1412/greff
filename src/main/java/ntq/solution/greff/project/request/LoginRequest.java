package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class LoginRequest {
    @NotBlank
    @NotNull(message = "email not null")
    @NotEmpty
    private String email;
    private String password;
}
