package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchTopPageRequest {
    private SearchByArea searchByArea;
    private Long[] searchByCategory;
}