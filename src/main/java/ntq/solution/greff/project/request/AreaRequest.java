package ntq.solution.greff.project.request;

import lombok.Data;

import java.util.List;

@Data
public class AreaRequest {
    private List<CityRequest> cityRequests;
}
