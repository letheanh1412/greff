package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ForgotRequest {
    @NotEmpty
    @NotNull(message = "email not null")
    @NotBlank
    private String email;
}
