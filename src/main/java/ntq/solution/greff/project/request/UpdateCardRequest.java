package ntq.solution.greff.project.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateCardRequest {
    private MultipartFile frontOfIdentityCard;
    private MultipartFile afterOfIdentityCard;
}
