package ntq.solution.greff.project.request;

import lombok.Data;

import java.util.Date;

@Data
public class SearchWorkerRequest {
    private String phone;
    private Date createdDate;
    private String invitationCode;
    private String email;
    private String name;
}
