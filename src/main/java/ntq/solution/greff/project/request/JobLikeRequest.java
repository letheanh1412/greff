package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class JobLikeRequest {
    private Long jobId;
    private String email;
    private boolean status;
}
