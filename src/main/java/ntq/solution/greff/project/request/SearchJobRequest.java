package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchJobRequest {
    private String search;
}
