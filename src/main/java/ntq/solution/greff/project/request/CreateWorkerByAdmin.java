package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CreateWorkerByAdmin {
    @NotEmpty
    @NotBlank
    @NotNull(message = "Name not null")
    private String name;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Furigana Name not null")
    private String furiganaName;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Phone Number not null")
    private String phoneNumber;
    @NotEmpty
    @NotBlank
    @Email
    @NotNull(message = "Email not null")
    private String email;
    private String password;
    private String codeConfirm;
    private Long areaId;
}
