
package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class WorkTimeRequest {
    private String workDate;
    private String workTime;
    private String workingTimeStart;
    private String workingTimeEnd;
}