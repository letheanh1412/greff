package ntq.solution.greff.project.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UpdateOccupationRequest {
    private List<String> nameStationList;
    private List<String> skillRequiredList;
    private List<String> specialityList;
    private Long categoryId;
    @NotEmpty
    @NotNull
    @NotBlank(message = "title not null")
    private String title;
    @NotEmpty
    @NotNull
    @NotBlank(message = "description not null")
    private String description;
    @NotEmpty
    @NotNull
    @NotBlank(message = "workAddress not null")
    private String workAddress;
    private String accessAddress;
    private String note;
    private String bringItems;
    private boolean status;
    private List<Long> routeId;
    private MultipartFile photos;
}
