package ntq.solution.greff.project.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JobRecruitingDTO {
    private Long id;
    private String linkImage;
    @JsonFormat(pattern = "MM-dd (E)")
    private String workDate;
    private String workAddress;
    private String workingTimeStart;
    private String workingTimeEnd;
    private String title;
    private String salary;
}
