package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class ListJobCategoryRequest {
    private String categoryName;
}
