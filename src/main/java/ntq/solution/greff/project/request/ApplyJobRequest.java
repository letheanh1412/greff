package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class ApplyJobRequest {
    private String name;
    private String furiganaName;
    private String phoneNumber;
    private String email;
    private Long jobId;
}
