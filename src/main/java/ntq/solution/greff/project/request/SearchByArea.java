package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchByArea {
    private Long[] areaId;
    private Long[] cityId;
    private Long[] stationId;
    private Long[] routeId;
}
