package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchDistrictRequest {
    private String katakanaName;
}
