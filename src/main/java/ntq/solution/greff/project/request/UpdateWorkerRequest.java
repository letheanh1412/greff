package ntq.solution.greff.project.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
public class UpdateWorkerRequest implements Serializable {
    private MultipartFile file;
    private String name;
    private String furiganaName;
    private String gender;
    private String birthday;
    private String phoneNumber;
    private Long areaId;
}
