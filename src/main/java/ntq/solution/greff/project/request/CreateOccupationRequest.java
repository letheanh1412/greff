package ntq.solution.greff.project.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CreateOccupationRequest {
    @NotBlank
    @NotNull(message = "Occupation Title not null")
    @NotEmpty
    private String title;
    @NotBlank
    @NotNull(message = "Description not null")
    @NotEmpty
    private String description;
    @NotBlank
    @NotNull(message = "Work address not null")
    @NotEmpty
    private String workAddress;
    private String accessAddress;
    private String note;
    private String bringItems;
    private boolean status;
    @NotNull(message = "Category Id not null")
    private Long categoryId;
    private MultipartFile photos;
    private List<String> nameRouteList;
    private List<Long> routeId;
    private List<String> skillRequiredList;
    private List<String> specialityList;
}
