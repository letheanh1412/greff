package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CreateCompanyRequest {
    @NotEmpty
    @NotBlank
    @NotNull(message = "companyName not null")
    private String companyName;
    private String companyNameKana;
    @NotEmpty
    @NotBlank
    @NotNull(message = "registerName not null")
    private String registerName;
    private String registerNameKana;
    private String room;
    private String building;
    @NotEmpty
    @NotBlank
    @NotNull(message = "zipcode not null")
    private String zipcode;
    @NotEmpty
    @NotBlank
    @NotNull(message = "hpUrl not null")
    private String hpUrl;
    @NotEmpty
    @NotBlank
    @NotNull(message = "nameContact not null")
    private String nameContact;
    @NotEmpty
    @NotBlank
    @NotNull(message = "phoneContact not null")
    private String phoneContact;
    @NotEmpty
    @NotBlank
    @NotNull(message = "emailContact not null")
    private String emailContact;
    private Boolean status;
    private String note;
    private String reason;
    @NotEmpty
    @NotBlank
    @NotNull(message = "city not null")
    private String city;
    @NotEmpty
    @NotBlank
    @NotNull(message = "district not null")
    private String district;
    List<String> listAreaRequests;
    List<String> listJobCategoryRequests;
}
