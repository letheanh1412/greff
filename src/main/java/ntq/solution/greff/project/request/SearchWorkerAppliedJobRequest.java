package ntq.solution.greff.project.request;

import lombok.Data;

@Data
public class SearchWorkerAppliedJobRequest {
    private String title;
}
