package ntq.solution.greff.project.request;

import lombok.Data;

import java.util.List;

@Data
public class CreateJobRequest {
    private String breakTime;
    private Long numberOfPeople;
    private Double salaryPerHours;
    private Long travelFees;
    private String deadlineToApplyDate;
    private String deadlineToApplyTime;
    private String recruitmentStatus;
    private String settingMatchingJob;
    private Long occupationId;
    List<WorkTimeRequest> workTimeList;
    private String workingTimeStart;
    private String workingTimeEnd;
}
