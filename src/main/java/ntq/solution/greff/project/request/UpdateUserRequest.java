package ntq.solution.greff.project.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UpdateUserRequest {
    @NotEmpty
    @NotBlank
    @NotNull(message = "Name not null")
    private String name;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Password not null")
    private String password;
    private boolean status;
}
