package ntq.solution.greff.project.request;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UpdateJobRequest {
    private String breakTime;
    @NotEmpty
    @NotBlank
    @NotNull(message = "numberOfPeople not null")
    private Integer numberOfPeople;
    @NotEmpty
    @NotBlank
    @NotNull(message = "salaryPerHours not null")
    private Double salaryPerHours;
    private Integer travelFees;
    private String recruitmentStatus;
    private String settingMatchingJob;
    private String deadlineToApplyDate;
    private String deadlineToApplyTime;
    @NotEmpty
    @NotBlank
    @NotNull(message = "Occupation not null")
    private Long occupationId;
    @NotEmpty
    @NotBlank
    @NotNull(message = "workDate not null")
    private String workDate;
    @NotEmpty
    @NotBlank
    @NotNull(message = "workTime not null")
    private String workTime;
    @NotEmpty
    @NotBlank
    @NotNull(message = "workingTimeStart not null")
    private String workingTimeStart;
    @NotEmpty
    @NotBlank
    @NotNull(message = "workingTimeEnd not null")
    private String workingTimeEnd;
}
