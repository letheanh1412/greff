package ntq.solution.greff.project.config;

import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new AuthenticationException("Email not exists") {
            };
        }
        if (user.isAccountLocked()) {
            throw new AuthenticationException("Email is Blocked") {
            };
        }
        return new CustomUserDetails(user);
    }
}
