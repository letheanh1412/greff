package ntq.solution.greff.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "skill_requireds")
public class SkillRequired {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name_skill")
    private String nameSkill;

    @ManyToOne
    @JoinColumn(name = "occupation_id", nullable = false)
    private Occupation occupation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillRequired that = (SkillRequired) o;
        return Objects.equals(id, that.id) &&
                nameSkill.equals(that.nameSkill) &&
                occupation.equals(that.occupation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameSkill, occupation);
    }
}
