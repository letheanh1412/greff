package ntq.solution.greff.project.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "job")
public class Job implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "break_time")
    private String breakTime;

    @NotNull
    @Column(name = "number_of_people")
    private Integer numberOfPeople;

    @NotNull
    @Column(name = "salary_per_hours")
    private Double salaryPerHours;

    @Column(name = "travel_fees")
    private Integer travelFees;

    @Column(name = "deadline_to_apply_date")
    private Date deadlineToApplyDateTime;

    @Column(name = "recruitment_status")
    private String recruitmentStatus;

    @Column(name = "title")
    private String title;

    @Column(name = "setting_matching_job")
    private String settingMatchingJob;

    @NotNull
    @Column(name = "work_date")
    private Date workDate;

    @NotNull
    @Column(name = "work_time")
    private String workTime;

    @NotNull
    @Column(name = "working_time_start")
    private String workingTimeStart;

    @NotNull
    @Column(name = "working_time_end")
    private String workingTimeEnd;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "occupation_id")
    private Occupation occupation;
}