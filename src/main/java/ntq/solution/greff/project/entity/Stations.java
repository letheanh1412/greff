package ntq.solution.greff.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "stations")
public class Stations implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "katakana_name")
    private String katakanaName;

    @Column(name = "kanji_name")
    private String kanjiName;

    @Column(name = "yomi")
    private String yomi;

    @Column(name = "type")
    private String type;

    @Column(name = "gcs")
    private String gcs;

    @Column(name = "lati_d")
    private String latiD;

    @Column(name = "longi_d")
    private String longiD;

    @Column(name = "lati")
    private String lati;

    @Column(name = "longi")
    private String longi;

    @Column(name = "code")
    private String code;

    @Column(name = "created_at")
    private String createdAt;

    @Column(name = "updated_at")
    private String updatedAt;

    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area area;
}
