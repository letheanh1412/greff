package ntq.solution.greff.project.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "worker")
@AllArgsConstructor
@NoArgsConstructor
public class Worker implements Serializable {
    @AllArgsConstructor
    @Getter
    public enum Status {
        STATUS_ENABLE(1, "Enable"),
        STATUS_DISABLE(0, "Disable");
        private final int value;
        private final String name;
    }

    @AllArgsConstructor
    @Getter
    public enum statusConfirmation {
        STATUS_NOT_UPLOAD(0, "NOT UPLOAD"),
        STATUS_WAITING_APPROVE(1, "WAITING APPROVE"),
        STATUS_APPROVE(2, "APPROVE");
        private final int value;
        private final String name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "furigana_name")
    private String furiganaName;

    @Column(name = "Gender")
    private String gender;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = " front_of_identity_card")
    private String frontOfIdentityCard;

    @Column(name = " after_of_identity_card")
    private String afterOfIdentityCard;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "code_comfirm")
    private String codeComfirm;

    @Column(name = "status_delete")
    private Boolean statusDelete;

    @Column(name = "status_register")
    private Boolean statusRegister;

    @Column(name = "status_confirmation")
    private Integer statusConfirmation;

    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area areaId;

    @OneToOne
    @JoinColumn(name = "user_email")
    private User user;
}
