package ntq.solution.greff.project.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "occupation")
public class Occupation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Size(max = 10000)
    @Column(name = "description")
    private String description;

    @Column(name = "workAddress")
    private String workAddress;

    @Column(name = "acess_address")
    private String accessAddress;

    @Column(name = "note")
    private String note;

    @Column(name = "bring_items")
    private String bringItems;

    @Column(name = "status")
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "job_category_id", nullable = false)
    private JobCategory jobCategory;

    @OneToMany(mappedBy = "occupation")
    private List<FileStorage> fileStorages = new ArrayList<>();
}
