package ntq.solution.greff.project.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_job")
public class UserJob implements Serializable {
    @AllArgsConstructor
    @Getter
    public enum statusApply {
        STATUS_NEW_REQUEST(0, "NEW REQUEST"),
        STATUS_CONTRACTED(1, "CONTRACTED"),
        STATUS_REJECTED(2, "REJECTED"),
        STATUS_CANCEL_BY_WORKER(3, "CANCEL_BY_WORKER"),
        STATUS_CANCEL_BY_STORE(4, "CANCEL_BY_STORE");
        private final int value;
        private final String name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "furigana_name")
    private String furiganaName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "status_apply")
    private Integer statusApply;

    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;

    @ManyToOne
    @JoinColumn(name = "worker_id")
    private Worker worker;
}

