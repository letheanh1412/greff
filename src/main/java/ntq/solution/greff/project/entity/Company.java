package ntq.solution.greff.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "company")
public class Company implements Serializable {
    private static final long serialVersionUID = -2576670215015463100L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Column(name = "company_name_kana")
    private String companyNameKana;

    @Column(name = "register_name", nullable = false)
    private String registerName;

    @Column(name = "register_name_kana")
    private String registerNameKana;

    @Column(name = "room")
    private String room;

    @Column(name = "building")
    private String building;

    @Column(name = "zip_code", nullable = false)
    private String zipcode;

    @Column(name = "hp_url", nullable = false)
    private String hpUrl;

    @Column(name = "name_contact")
    private String nameContact;

    @Column(name = "phone_contact")
    private String phoneContact;

    @Column(name = "email_contact")
    private String emailContact;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "note")
    private String note;

    @Column(name = "reason")
    private String reason;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "district", nullable = false)
    private String district;

}
