package ntq.solution.greff.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "store")
public class Store implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "store_name")
    private String storeName;

    @Column(name = "store_name_kana")
    private String storeNameKana;

    @Column(name = "district")
    private String district;

    @Column(name = "address")
    private String address;

    @Column(name = "hp_url")
    private String hpUrl;

    @Column(name = "email_curator")
    private String emailCurator;

    @Column(name = "name_curator")
    private String nameCurator;

    @Column(name = "phone_curator")
    private String phoneCurator;

    @Column(name = "status")
    private Boolean status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private Company company;
}
