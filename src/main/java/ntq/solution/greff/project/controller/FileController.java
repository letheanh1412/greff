package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.entity.FileStorage;
import ntq.solution.greff.project.repository.FileStorageRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/file/")
public class FileController {

    private final FileStorageRepository fileStorageRepository;

    public FileController(FileStorageRepository fileStorageRepository) {
        this.fileStorageRepository = fileStorageRepository;
    }


    @GetMapping(value = "/image/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    private ResponseEntity<byte[]> getImageOccupation(@PathVariable Long id) {
        FileStorage fileStorage = fileStorageRepository.findById(id).get();
        return ResponseEntity.ok(fileStorage.getPhotos());
    }
}
