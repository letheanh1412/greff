package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.CompanyDetailDto;
import ntq.solution.greff.project.response.dto.CompanyShowListDto;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.CompanyService;
import org.mapstruct.Context;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("")
    private ResponseEntity<Page<CompanyShowListDto>> search(@ModelAttribute SearchCompanyRequest searchCompanyRequest,
                                                            @PageableDefault(size = 200, sort = "id",
                                                                    direction = Sort.Direction.DESC) Pageable pageable) {
        return companyService.search(searchCompanyRequest, pageable);
    }

    @PostMapping("")
    private ResponseEntity<CompanyShowListDto> create(@Valid @RequestBody CreateCompanyRequest createCompanyRequest) throws Exception {
        return companyService.create(createCompanyRequest);
    }

    @PutMapping("/{id}")
    private ResponseEntity<CompanyShowListDto> edit(@PathVariable Long id,
                                                    @Valid @RequestBody UpdateCompanyRequest updateCompanyRequest) throws Exception {
        return companyService.edit(id, updateCompanyRequest);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyDetailDto> detail(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.details(id));
    }

    @GetMapping("/companies-csv")
    public void downloadCompaniesCSV(@Context HttpServletResponse httpServletResponse) {
        companyService.generateCsvResponse(httpServletResponse);
    }

    @GetMapping("/users")
    public ResponseEntity<Page<UserDTO>> getCompanyUserStore(
            @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.ASC)
                    Pageable pageable) {
        return ResponseEntity.ok(companyService.getUserStore(pageable));
    }

    @PostMapping("/users")
    private ResponseEntity<UserDTO> createUserCompany(@Valid @RequestBody CreateUserRequest createUserRequest)
            throws Exception {
        return ResponseEntity.ok(companyService.createUserCompany(createUserRequest));
    }

    @PutMapping("/users/{email}")
    private ResponseEntity<UserDTO> updateUserStore(@PathVariable String email,

                                                    @Valid @RequestBody UpdateUserRequest updateUserRequest) throws Exception {
        return ResponseEntity.ok(companyService.updateUserCompany(email, updateUserRequest));
    }

    @DeleteMapping("/users/{email}")
    private ResponseEntity<Boolean> deleteUserCompany(@PathVariable String email) throws Exception {
        if (companyService.deleteUserCompany(email)) {
            return ResponseEntity.ok(true);
        }
        return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("users/{email}")
    private ResponseEntity<UserDTO> detailUserCompany(@PathVariable String email) {
        return ResponseEntity.ok(companyService.detailUserCompany(email));
    }

    @GetMapping("/account")
    public ResponseEntity<Page<UserDTO>> getAllAccount(Pageable pageable) {
        return ResponseEntity.ok(companyService.getAllAccountCompany(pageable));
    }
}
