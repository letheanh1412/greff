package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.response.dto.RouteDTO;
import ntq.solution.greff.project.response.dto.StationsDTO;
import ntq.solution.greff.project.service.StationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class StationController {

    private final StationService stationService;

    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    @GetMapping("/station")
    public Page<StationsDTO> getAllStation(@PageableDefault(size = 20) Pageable pageable) {
        return stationService.getAllStation(pageable);
    }

    @GetMapping("/station/routes")
    public Page<RouteDTO> getRouteByStation(@RequestParam("name") String name,
                                            @PageableDefault(size = 20) Pageable pageable) {
        return stationService.getRouteByStation(name, pageable);
    }
}
