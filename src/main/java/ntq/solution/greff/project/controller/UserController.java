package ntq.solution.greff.project.controller;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.request.CreateUserRequest;
import ntq.solution.greff.project.request.ForgotRequest;
import ntq.solution.greff.project.response.dto.ChangePasswordDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("")
    private ResponseEntity<UserDTO> create(@Valid @RequestBody CreateUserRequest createUserRequest) {
        try {
            return ResponseEntity.ok(userService.create(createUserRequest));
        } catch (Exception e) {
            log.error("Create user Error: " + e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/forgot")
    private ResponseEntity<String> forgot(@RequestBody ForgotRequest forgotRequest) {
        try {
            return userService.forgot(forgotRequest);
        } catch (Exception e) {
            log.error("Forgot Error: " + e);
            return new ResponseEntity<>("Your login email is incorrect. Please try enter!", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/change-password")
    public ResponseEntity<User> requestReset(@RequestBody ChangePasswordDTO changePasswordDTO, String email) {
        return ResponseEntity.ok(userService.changePassWord(changePasswordDTO, email));
    }
}
