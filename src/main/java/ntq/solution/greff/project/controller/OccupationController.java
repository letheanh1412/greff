package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.CreateOccupationRequest;
import ntq.solution.greff.project.request.UpdateOccupationRequest;
import ntq.solution.greff.project.response.dto.OccupationDTO;
import ntq.solution.greff.project.service.OccupationService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/occupation")
public class OccupationController {

    private final OccupationService occupationService;

    public OccupationController(OccupationService occupationService) {
        this.occupationService = occupationService;
    }

    @GetMapping("")
    private ResponseEntity<Page<OccupationDTO>> show(
            @PageableDefault(size = 20, sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        return ResponseEntity.ok(occupationService.show(pageable));
    }

    @PostMapping("")
    private ResponseEntity<OccupationDTO> create(@Valid @ModelAttribute CreateOccupationRequest
                                                         createOccupationRequest) throws Exception {
        return ResponseEntity.ok(occupationService.create(createOccupationRequest));
    }

    @PutMapping("/{id}")
    private ResponseEntity<OccupationDTO> update(@PathVariable Long id,
                                                 @Valid @ModelAttribute UpdateOccupationRequest
                                                         updateOccupationRequest) throws Exception {
        return ResponseEntity.ok(occupationService.edit(id, updateOccupationRequest));
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Boolean> delete(@PathVariable Long id) throws Exception {
        return ResponseEntity.ok(occupationService.delete(id));
    }

    @GetMapping("/{id}")
    private ResponseEntity<OccupationDTO> details(@PathVariable Long id) {
        return ResponseEntity.ok(occupationService.detail(id));
    }

    @RequestMapping(value = "/uploadImage2", method = RequestMethod.POST)
    public String uploadImage2(@RequestBody String imageValue) {
        try {

            //This will decode the String which is encoded by using Base64 class
            byte[] imageByte = Base64.decodeBase64(imageValue);
            return "succses";
        } catch (Exception e) {
            System.out.println("thất bại");
            return null;
        }
    }

}
