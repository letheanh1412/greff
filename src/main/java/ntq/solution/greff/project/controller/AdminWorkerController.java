package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.CreateWorkerByAdmin;
import ntq.solution.greff.project.request.EditWorkerRequest;
import ntq.solution.greff.project.response.dto.WorkerDetailDTO;
import ntq.solution.greff.project.response.dto.WorkerShowListDTO;
import ntq.solution.greff.project.service.WorkerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/api")
public class AdminWorkerController {
    private final WorkerService workerService;

    public AdminWorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/admin/worker/search")
    private ResponseEntity<Page<WorkerShowListDTO>> searchWorker(@Param("search") String search,
                                                                 String startDate,
                                                                 String endDate,
                                                                 @PageableDefault(size = 20, sort = "id",
                                                                         direction = Sort.Direction.DESC)
                                                                         Pageable pageable) throws Exception {
        Page<WorkerShowListDTO> contents = workerService.searchWorker(search, startDate, endDate, pageable);
        return ResponseEntity.ok(contents);
    }

    @DeleteMapping("/admin/worker/delete/{id}")
    private ResponseEntity<Boolean> deleteWorker(@PathVariable Long id) {
        return ResponseEntity.ok(workerService.deleteWorker(id));
    }

    @GetMapping("/admin/worker/detail/{id}")
    private ResponseEntity<WorkerDetailDTO> detailWorker(@PathVariable Long id) {
        return ResponseEntity.ok(workerService.detailWorker(id));
    }

    @PutMapping("/admin/worker/edit/{id}")
    private ResponseEntity<WorkerDetailDTO> editWorker(@PathVariable Long id,
                                                       @Valid @RequestBody EditWorkerRequest editWorkerRequest)
            throws ParseException {
        return ResponseEntity.ok(workerService.editWorker(id, editWorkerRequest));
    }

    @GetMapping("/admin/worker/create")
    private ResponseEntity<WorkerDetailDTO> createWorker(@Valid @RequestBody CreateWorkerByAdmin
                                                                 createWorkerByAdmin) {
        return ResponseEntity.ok(workerService.createWorkerByAdmin(createWorkerByAdmin));
    }
}
