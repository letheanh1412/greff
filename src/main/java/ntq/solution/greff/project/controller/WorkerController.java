package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.*;
import ntq.solution.greff.project.response.dto.ApplyJobDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.response.dto.WorkerDTO;
import ntq.solution.greff.project.service.JobService;
import ntq.solution.greff.project.service.WorkerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/worker")
public class WorkerController {

    private final WorkerService workerService;

    private final JobService jobService;

    public WorkerController(WorkerService workerService, JobService jobService) {
        this.workerService = workerService;
        this.jobService = jobService;
    }

    @PostMapping("/register")
    private ResponseEntity<WorkerDTO> createWorker(@Valid @RequestBody CreateWorkerRequest createWorkerRequest)
            throws Exception {
        return ResponseEntity.ok(workerService.createWorker(createWorkerRequest));
    }

    @PostMapping("/check-code/{id}")
    private ResponseEntity<WorkerDTO> check(@PathVariable Long id,
                                            @Valid @RequestBody CheckCodeRequest checkCodeRequest) throws Exception {
        return ResponseEntity.ok(workerService.checkCodeConfirm(id, checkCodeRequest));
    }

    @PostMapping("/send-code-agian")
    private ResponseEntity<WorkerDTO> sendCodeAgian(@RequestBody CreateWorkerRequest createWorkerRequest) {
        return ResponseEntity.ok(workerService.sendCodeAgain(createWorkerRequest));
    }

    @PostMapping("/set-password/{id}")
    private ResponseEntity<WorkerDTO> setPassword(@PathVariable Long id,
                                                  @Valid @RequestBody UpdatePassword updatePassword) throws Exception {
        return ResponseEntity.ok(workerService.setPassword(id, updatePassword));
    }

    @PostMapping("/forgot/send-code")
    private ResponseEntity<WorkerDTO> fogotPassword(@RequestBody ForgotPasswordWorkerRequest forgotPasswordWorkerRequest) {
        return ResponseEntity.ok(workerService.getMailMessage(forgotPasswordWorkerRequest));
    }

    @PostMapping("/forgot/check-code/{id}")
    private ResponseEntity<WorkerDTO> checkCodeForgot(@PathVariable Long id,
                                                      @Valid @RequestBody CheckCodeRequest checkCodeRequest) throws Exception {
        return ResponseEntity.ok(workerService.checkCodeConfirm(id, checkCodeRequest));
    }

    @PostMapping("/forgot/set-password/{id}")
    private ResponseEntity<WorkerDTO> setPasswordForgot(@PathVariable Long id,
                                                        @Valid @RequestBody UpdatePassword updatePassword) throws Exception {
        return ResponseEntity.ok(workerService.setPassword(id, updatePassword));
    }

    @GetMapping("/job-like")
    private ResponseEntity<Page<JobShowListDTO>> getJobLike(@PathVariable String id,
                                                            @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(jobService.showJobLike(id, pageable));
    }

    @PostMapping("/apply-job")
    private ResponseEntity<ApplyJobDTO> edit(@RequestBody ApplyJobRequest applyJobRequest) {
        return ResponseEntity.ok(workerService.applyJob(applyJobRequest));
    }
}
