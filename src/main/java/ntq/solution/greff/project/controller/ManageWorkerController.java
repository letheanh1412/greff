package ntq.solution.greff.project.controller;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.response.dto.ApproveRejectDTO;
import ntq.solution.greff.project.service.WorkerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin/worker")
@Slf4j
public class ManageWorkerController {
    private final WorkerService workerService;

    public ManageWorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @PostMapping("/change-status")
    private ResponseEntity<ApproveRejectDTO> create(@Valid @RequestBody Integer StatusConfirmation,
                                                    @RequestBody Long idWorker) {
        return ResponseEntity.ok(workerService.StatusConfirmation(StatusConfirmation, idWorker));
    }
}
