package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.CreateJobCategoryRequest;
import ntq.solution.greff.project.request.UpdateJobCategoryRequest;
import ntq.solution.greff.project.response.dto.JobCategoryDto;
import ntq.solution.greff.project.service.JobCategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/category")
public class JobCategoryController {

    private final JobCategoryService jobCategoryService;

    public JobCategoryController(JobCategoryService jobCategoryService) {
        this.jobCategoryService = jobCategoryService;
    }

    @GetMapping("")
    private ResponseEntity<Page<JobCategoryDto>> search(@RequestParam(defaultValue = "") String categoryName,
                                                        Pageable pageable) {

        return ResponseEntity.ok(jobCategoryService.search(categoryName, pageable));
    }

    @PostMapping("")
    private ResponseEntity<JobCategoryDto> create(@Valid @RequestBody CreateJobCategoryRequest
                                                          createJobCategoryRequest) throws Exception {
        return ResponseEntity.ok(jobCategoryService.create(createJobCategoryRequest));
    }

    @PutMapping("/{id}")
    private ResponseEntity<JobCategoryDto> edit(@PathVariable Long id, @Valid @RequestBody
            UpdateJobCategoryRequest updateJobCategoryRequest) throws Exception {
        return ResponseEntity.ok(jobCategoryService.edit(id, updateJobCategoryRequest));
    }
}
