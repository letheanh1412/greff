package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.response.dto.AcceptRejectDTO;
import ntq.solution.greff.project.response.dto.DetailWorkerAppliedJobDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.response.dto.WorkerAppliedJobDTO;
import ntq.solution.greff.project.service.WorkerAppliedJobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/worker-applied-job")
public class WorkerAppliedJobController {

    private final WorkerAppliedJobService workerAppliedJobService;

    public WorkerAppliedJobController(WorkerAppliedJobService workerAppliedJobService) {
        this.workerAppliedJobService = workerAppliedJobService;
    }

    @GetMapping("/{id}")
    private ResponseEntity<Page<WorkerAppliedJobDTO>> showListWorkerAppliedJob(@PathVariable Long id,
                                                                               @PageableDefault(size = 20, sort = "id",
                                                                                       direction = Sort.Direction.DESC)
                                                                                       Pageable pageable) {
        return ResponseEntity.ok(workerAppliedJobService.showListWorkerAppliedJob(id, pageable));
    }

    @GetMapping("/detail/{id}")
    private ResponseEntity<DetailWorkerAppliedJobDTO> detailWorkerAppliedJob(@PathVariable Long id) {
        return ResponseEntity.ok(workerAppliedJobService.detailWorkerAppliedJob(id));
    }


    @GetMapping("/job-apply/{id}")
    private ResponseEntity<Page<JobShowListDTO>> showListJobApply(@PathVariable String id,
                                                                  @RequestParam(defaultValue = "") Integer statusApply,
                                                                  @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(workerAppliedJobService.showListJobApply(id, statusApply, pageable));
    }

    @PostMapping("/change-status")
    private ResponseEntity<AcceptRejectDTO> create(@Valid @RequestBody Integer statusApplyJob, @RequestBody Long id) {
        return ResponseEntity.ok(workerAppliedJobService.statusApplyJob(statusApplyJob, id));
    }
}
