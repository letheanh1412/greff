package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.JobRecruitingDTO;
import ntq.solution.greff.project.request.SearchTopPageRequest;
import ntq.solution.greff.project.response.CityResponse;
import ntq.solution.greff.project.response.RouteResponse;
import ntq.solution.greff.project.response.StationResponse;
import ntq.solution.greff.project.response.dto.SpecialityDTO;
import ntq.solution.greff.project.response.dto.TopPageJobCategoryDTO;
import ntq.solution.greff.project.service.TopPageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/top-page")
public class TopPageController {

    private final TopPageService topPageService;

    public TopPageController(TopPageService topPageService) {
        this.topPageService = topPageService;
    }

    @PostMapping(value = {"", "/"})
    public ResponseEntity<Page<JobRecruitingDTO>> searchTopPageBy(
            @RequestBody SearchTopPageRequest searchTopPageRequest, @PageableDefault(size = 20, sort = "id",
            direction = Sort.Direction.DESC) Pageable pageable) {
        return ResponseEntity.ok(topPageService.searchTopPage(searchTopPageRequest, pageable));
    }

    @GetMapping("/getAllJobCategory")
    public ResponseEntity<List<TopPageJobCategoryDTO>> getAllJobCategoryAndTotalJob() {
        return ResponseEntity.ok(topPageService.getJobByCategory());
    }

    @GetMapping("/search-area")
    public ResponseEntity<SpecialityDTO> speciatily() {
        return null;
    }

    @GetMapping("/get-city")
    private List<CityResponse> getCityByArea(@RequestParam("katakanaName") String katakanaName) {
        return topPageService.getCityByArea(katakanaName);
    }

    @GetMapping("/get-station")
    private List<StationResponse> getStationByCity(@RequestParam("katakanaName") String katakanaName) {
        return topPageService.getStationByCity(katakanaName);
    }

    @GetMapping("/get-route")
    private Page<RouteResponse> getRouteByCity(@RequestParam("katakanaName") String katakanaName,
                                               @PageableDefault(size = 20) Pageable pageable) {
        return topPageService.getRouteByStation(katakanaName, pageable);
    }
}
