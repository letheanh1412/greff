package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.response.dto.AreaDTO;
import ntq.solution.greff.project.service.AreaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class AreaController {

    private final AreaService areaService;

    public AreaController(AreaService areaService) {
        this.areaService = areaService;
    }

    @GetMapping("/area")
    public ResponseEntity<List<AreaDTO>> getAllArea() {
        try {
            return ResponseEntity.ok(areaService.getAllArea());
        } catch (Exception e) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/city")
    public ResponseEntity<List<AreaDTO>> getAllCity() {
        try {
            return ResponseEntity.ok(areaService.getCity());
        } catch (Exception e) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district")
    public ResponseEntity<Page<AreaDTO>> getDistrictByCity(@ModelAttribute("katakanaNameCity") String katakanaNameCity,
                                                           @PageableDefault(size = 5) Pageable pageable) {
        try {
            return ResponseEntity.ok(areaService.getDistrict(katakanaNameCity, pageable));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
