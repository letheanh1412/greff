package ntq.solution.greff.project.controller;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.request.UpdateCardRequest;
import ntq.solution.greff.project.request.UpdateWorkerRequest;
import ntq.solution.greff.project.response.dto.MyPageDTO;
import ntq.solution.greff.project.response.dto.UpdateCartDTO;
import ntq.solution.greff.project.response.dto.UploadFileDTO;
import ntq.solution.greff.project.service.WorkerService;
import ntq.solution.greff.project.service.impl.DocumentStorageServiceImpl;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/my-page")
@Slf4j
public class MyPageController {
    private final WorkerService workerService;
    private final DocumentStorageServiceImpl documentStorageService;

    public MyPageController(WorkerService workerService, DocumentStorageServiceImpl documentStorageService) {
        this.workerService = workerService;
        this.documentStorageService = documentStorageService;
    }

    @GetMapping("")
    private ResponseEntity<MyPageDTO> InfoWorker() {
        return ResponseEntity.ok(workerService.infoWorker());
    }

    @PutMapping("/update-worker")
    private ResponseEntity<MyPageDTO> edit(@ModelAttribute UpdateWorkerRequest updateWorkerRequest) throws ParseException {
        return ResponseEntity.ok(workerService.updateWorker(updateWorkerRequest));
    }

    @PostMapping("/update-card")
    private ResponseEntity<UpdateCartDTO> edit(@ModelAttribute UpdateCardRequest updateCardRequest) {
        return ResponseEntity.ok(workerService.updateCard(updateCardRequest));
    }

    @PostMapping("/upload_degree")
    public ResponseEntity<List<UploadFileDTO>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return ResponseEntity.ok(documentStorageService.uploadMultipleFiles(files));
    }

    @GetMapping("/upload/{filename:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) throws Exception {
        Resource resource = documentStorageService.loadFileAsResource(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + resource.getFilename() + "\"")
                .body(resource);
    }
}
