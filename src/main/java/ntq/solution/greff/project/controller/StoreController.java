package ntq.solution.greff.project.controller;

import ntq.solution.greff.project.request.CreateStoreRequest;
import ntq.solution.greff.project.request.CreateUserStoreRequest;
import ntq.solution.greff.project.request.EditStoreRequest;
import ntq.solution.greff.project.response.dto.StoreDTO;
import ntq.solution.greff.project.response.dto.UserDTO;
import ntq.solution.greff.project.service.StoreService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/store")
public class StoreController {

    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping("")
    public ResponseEntity<Page<StoreDTO>> search(@Param("search") String search, Pageable pageable) {
        return ResponseEntity.ok(storeService.search(search, pageable));
    }

    @PostMapping("/")
    public ResponseEntity<StoreDTO> create(@RequestBody CreateStoreRequest createStoreRequest) {
        return ResponseEntity.ok(storeService.create(createStoreRequest));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StoreDTO> edit(@PathVariable Long id, @RequestBody EditStoreRequest editStoreRequest) {
        return ResponseEntity.ok(storeService.edit(id, editStoreRequest));
    }

    @PostMapping("/users")
    private ResponseEntity<UserDTO> createUserStore(@RequestBody CreateUserStoreRequest createUserStoreRequest) {
        return ResponseEntity.ok(storeService.createUserStore(createUserStoreRequest));
    }
}
