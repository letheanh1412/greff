package ntq.solution.greff.project.controller;

import lombok.extern.slf4j.Slf4j;
import ntq.solution.greff.project.request.CreateJobRequest;
import ntq.solution.greff.project.request.JobLikeRequest;
import ntq.solution.greff.project.request.UpdateJobRequest;
import ntq.solution.greff.project.response.dto.JobDetailDTO;
import ntq.solution.greff.project.response.dto.JobConfirmDTO;
import ntq.solution.greff.project.response.dto.JobLikeDTO;
import ntq.solution.greff.project.response.dto.JobShowListDTO;
import ntq.solution.greff.project.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/job")
@Slf4j
public class JobController {
    private final JobService jobService;

    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping("")
    private ResponseEntity<Page<JobShowListDTO>> search(
            @RequestParam(name = "search") String search,
            @RequestParam(name = "status") String status,
            @PageableDefault(size = 20) Pageable pageable
    ) {
        return ResponseEntity.ok(jobService.search(search, status, pageable));
    }

    @PostMapping("")
    private ResponseEntity<List<JobConfirmDTO>> create(@Valid @RequestBody CreateJobRequest createJobRequest)
            throws Exception {
        return ResponseEntity.ok(jobService.create(createJobRequest));
    }

    @PutMapping("/{id}")
    private ResponseEntity<JobShowListDTO> edit(@PathVariable Long id, @RequestBody UpdateJobRequest updateJobRequest) {
        try {
            return ResponseEntity.ok(jobService.edit(id, updateJobRequest));
        } catch (Exception e) {
            log.error("Update job Error: " + e);
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/job-detail/{id}")
    private ResponseEntity<JobDetailDTO> details(@PathVariable Long id) {
        return ResponseEntity.ok(jobService.detail(id));
    }

    @PostMapping("/like-job")
    private ResponseEntity<JobLikeDTO> likeJob(@RequestBody JobLikeRequest jobLikeRequest) {
        return ResponseEntity.ok(jobService.jobLike(jobLikeRequest));
    }

}
