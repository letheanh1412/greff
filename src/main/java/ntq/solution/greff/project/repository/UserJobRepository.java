package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.UserJob;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJobRepository extends JpaRepository<UserJob, Long> {
    Page<UserJob> findAllByJobId(Long id, Pageable pageable);

    Page<UserJob> findAllByWorkerUserEmailAndStatusApplyOrderByJobWorkDateAsc(String email, Integer statusApply, Pageable pageable);
}
