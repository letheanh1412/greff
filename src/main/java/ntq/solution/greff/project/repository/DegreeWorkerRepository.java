package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.DegreeWorker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DegreeWorkerRepository extends JpaRepository<DegreeWorker, Long> {
    List<DegreeWorker> findAllByWorkerId(Long id);
}
