package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.CompanyJobCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyJobCategoryRepository extends JpaRepository<CompanyJobCategory, Long> {
    List<CompanyJobCategory> findAllByCompanyId(Long id);
}
