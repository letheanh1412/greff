package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.User;
import ntq.solution.greff.project.entity.Worker;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkerRepository extends JpaRepository<Worker, Long> {

    Worker findByUser(User user);

    @Query(value = "select * from worker w join users u on u.email = w.user_id where w.status_delete = true " +
            "and (w.name like ('%' :search '%') or u.email like ('%' :search '%')" +
            "or w.phone_number like ('%' :search '%'))" +
            "and (date(w.create_date) between :startDate and :endDate)", nativeQuery = true)
    Page<Worker> getAllWorkers(@Param("search") String search, @Param("startDate") String startDate,
                               @Param("endDate") String endDate, Pageable pageable);

    @Query(value = "select * from worker w join users u on u.email = w.user_id where w.status_delete = true and " +
            "date(w.create_date) = current_date()", nativeQuery = true)
    List<Worker> getAllWorkersToDay();

    @Query(value = "select * from worker w join users u on u.email = w.user_id where w.status_delete = true",
            nativeQuery = true)
    List<Worker> getAllWorkersTotal();

    Worker findByUserEmail(String email);
}
