package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Routes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends JpaRepository<Routes, Long> {
    Routes findByName(String name);
}
