package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.entity.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
    List<Speciality> findAllByOccupation(Occupation occupation);

    List<Speciality> findAllByOccupationId(Long id);
}
