package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Job;
import ntq.solution.greff.project.entity.Occupation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
    @Query("select job from Job job where job.occupation.jobCategory.id in (?1)")
    Page<Job> searchJobByJobCategory(Long[] categoryId, Pageable pageable);

    long countAllByOccupation_JobCategory_Id(Long id);

    long countAllByOccupation(Occupation occupation);

    @Query("select e from Job e inner join Occupation occ on occ.id=e.occupation.id " +
            "where e.title like concat('%', ?1, '%')" +
            "or concat(e.workDate,e.workTime) like concat('%', ?1, '%')" +
            "and e.recruitmentStatus like concat('%', ?2, '%')")
    Page<Job> findByTitleOrWorkTimeAndAndRecruitmentStatus(@Param("search") String search, @Param("status")
            String status, Pageable pageable);

    @Query(value = "SELECT job from Job job INNER JOIN Occupation oc ON job.occupation.id = oc.id " +
            "INNER JOIN OccupationRoutes ocrt ON oc.id = ocrt.occupation.id " +
            "INNER JOIN Routes rt ON ocrt.routes.id = rt.id " +
            "INNER JOIN AreaRoute art ON rt.id = art.routes.id " +
            "INNER JOIN Stations st ON art.id = st.area.id " +
            "WHERE st.area.id in (?1) or st.id in (?2) or rt.id in (?3) GROUP BY oc.id")
    Page<Job> getJobTopPage(Long[] areaId, Long[] stationId, Long[] routeId, Pageable pageable);
}
