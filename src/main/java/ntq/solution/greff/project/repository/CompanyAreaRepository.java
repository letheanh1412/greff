package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.CompanyArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyAreaRepository extends JpaRepository<CompanyArea, Long> {
    List<CompanyArea> findAllByCompanyId(Long id);
}
