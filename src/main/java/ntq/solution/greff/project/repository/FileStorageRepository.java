package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.FileStorage;
import ntq.solution.greff.project.entity.Occupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileStorageRepository extends JpaRepository<FileStorage, Long> {
    List<FileStorage> findAllByOccupationId(Long id);

    List<FileStorage> findAllByOccupation(Occupation occupation);
}
