package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Area;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AreaRepository extends JpaRepository<Area, Long> {

    List<Area> findAllByLevel(Integer level);

    Area findByKatakanaNameLikeAndLevel(String katakanaName, Integer level);

    Area findByNameAndLevel(String name, Integer level);

    Page<Area> findAllByLevelAndParentId(Integer level, Long parentId, Pageable pageable);

    List<Area> findAllByLevelAndParentId(Integer level, Long parentId);

    Area findByName(String name);
}
