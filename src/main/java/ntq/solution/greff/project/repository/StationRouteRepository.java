package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.StationRoute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRouteRepository extends JpaRepository<StationRoute, Long> {
    Page<StationRoute> findAllByStationsId(Long id, Pageable pageable);
}
