package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.OccupationStations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OccupationStationRepository extends JpaRepository<OccupationStations, Long> {
}
