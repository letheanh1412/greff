package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.StoreRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRouteRepository extends JpaRepository<StoreRoute, Long> {
}
