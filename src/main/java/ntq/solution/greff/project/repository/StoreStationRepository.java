package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.StoreStation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreStationRepository extends JpaRepository<StoreStation, Long> {
    List<StoreStation> findAllByStoreId(Long Id);
}
