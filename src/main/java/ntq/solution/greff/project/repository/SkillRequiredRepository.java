package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Occupation;
import ntq.solution.greff.project.entity.SkillRequired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRequiredRepository extends JpaRepository<SkillRequired, Long> {
    List<SkillRequired> findAllByOccupation(Occupation occupation);

    List<SkillRequired> findAllByOccupationId(Long occupationId);
}
