package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User findByEmail(String email);

    Page<User> findAllByCompanyIdNotNull(Pageable pageable);


}
