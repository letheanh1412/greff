package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    List<UserRole> findAllByUserEmail(String email);

    Page<UserRole> findAllByRoleRole(String role, Pageable pageable);
}
