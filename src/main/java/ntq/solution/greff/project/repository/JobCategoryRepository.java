package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.JobCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobCategoryRepository extends JpaRepository<JobCategory, Long> {
    JobCategory findByCategoryName(String categoryName);

    @Query(value = "select jc from JobCategory jc where jc.categoryName like concat ( '%' ,:categoryName , '%')")
    Page<JobCategory> getAllCategoryByCategoryName(@Param("categoryName") String categoryName, Pageable pageable);

    Page<JobCategory> findAllByCategoryNameContaining(String categoryName, Pageable pageable);
}
