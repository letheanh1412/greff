package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.JobLike;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobLikeRepository extends JpaRepository<JobLike, Long> {
    JobLike findByJobId(Long jobId);

    Page<JobLike> findAllByUserEmailOrderByCreatAtDesc(String userId, Pageable pageable);
}
