package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.OccupationRoutes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OccupationRoutesRepository extends JpaRepository<OccupationRoutes, Long> {
    List<OccupationRoutes> findAllByRoutesId(Long id);

    List<OccupationRoutes> findAllByOccupationId(Long id);

    @Query("select ocrt from OccupationRoutes ocrt where ocrt.routes.id in (?1)")
    List<OccupationRoutes> getAllOccupationByRoute(Long[] routeId);
}
