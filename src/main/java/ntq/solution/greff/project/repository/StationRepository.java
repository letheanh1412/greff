package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Area;
import ntq.solution.greff.project.entity.Stations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StationRepository extends JpaRepository<Stations, Long> {
    Stations findByNameOrKanjiNameOrKatakanaName(String name, String KanjiName, String KatakanaName);

    Stations findByKatakanaNameLike(String katakanaName);

    List<Stations> findAllByArea(Area area);
}
