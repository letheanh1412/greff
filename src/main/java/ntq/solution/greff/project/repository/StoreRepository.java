package ntq.solution.greff.project.repository;

import ntq.solution.greff.project.entity.Job;
import ntq.solution.greff.project.entity.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
    @Query("select e from Store e where e.company.id = ?1 " +
            "and( e.storeName like concat('%', ?2, '%')" +
            "or e.emailCurator like concat('%', ?2, '%')" +
            "or e.phoneCurator like concat('%', ?2, '%')" +
            "or e.nameCurator like concat('%', ?2, '%')" +
            "or e.storeNameKana like concat('%', ?2, '%')" +
            "or e.address like concat('%', ?2, '%') )")
    Page<Store> findBySearch(Long companyId, String search, Pageable pageable);
}
